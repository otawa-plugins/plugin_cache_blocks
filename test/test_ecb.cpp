#include "catch.hpp"

#include <otawa/app/Application.h>
#include <otawa/cfg/features.h>
#include <otawa/script/Script.h>
#include <otawa/prog/WorkSpace.h>
#include <otawa/proc/DynProcessor.h>
#include <otawa/cfg/Dominance.h>
#include <otawa/proc/DynFeature.h>

//includes pour l'affichage du CFG
#include <otawa/display/CFGOutput.h>
#include <elm/io/OutFileStream.h>
#include <otawa/flowfact/features.h>
#include <classic_ecb.h>

using namespace otawa; //comme import
using namespace otawa::classic_ecb;
using namespace Mathset;

TEST_CASE("ECB","ecb") {
  	WorkSpace *ws = NULL;
	PropList conf;
	Manager manager;
	NO_SYSTEM(conf) = true;
	TASK_ENTRY(conf) = "main";
	VERBOSE(conf) = true;
	CACHE_CONFIG_PATH(conf) = "./cache.xml";
	FLOW_FACTS_PATH(conf) = "test_1.ff";
        ws = manager.load("test_1", conf);
	ws->require(DynFeature("otawa::classic_ecb::CLASSIC_ECB_PROCESSOR"), conf);
	Multiset<int> *ecbs = ECBS(ws);

	int i = 0;

	ecbs->print_vector();
	REQUIRE((*ecbs)[i++] == 4);
	std::cout << "i:" << i << std::endl;
        REQUIRE((*ecbs)[i++] == 5);
	REQUIRE((*ecbs)[i++] == 12);
	REQUIRE((*ecbs)[i++] == 6);
	REQUIRE((*ecbs)[i++] == 7);
	REQUIRE((*ecbs)[i++] == 8);
	REQUIRE((*ecbs)[i++] == 8);
	REQUIRE((*ecbs)[i++] == 9);
	REQUIRE((*ecbs)[i++] == 10);
	REQUIRE((*ecbs)[i++] == 11);
	REQUIRE((*ecbs)[i++] == 13);

	for(int j = i + 10; i < j;) {
	  REQUIRE((*ecbs)[i++] == 0);
	}

	for(int j = i + 10; i < j;) {
	  REQUIRE((*ecbs)[i++] == 1);
	}

	for(int j = i + 10; i < j;) {
	  REQUIRE((*ecbs)[i++] == 2);
	}

	for(int j = i + 10; i < j;) {
	  REQUIRE((*ecbs)[i++] == 3);
	}
}
