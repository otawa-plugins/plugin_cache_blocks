# OTAWA plugin for computing cache block usage #

Author: Fabien Bouquillon 

## Instructions ##

### OTAWA v2 ###

See https://gitlab.cristal.univ-lille.fr/otawa-plugins/otawa-doc for
instructions on how to install OTAWA v2.

### Setting environment ###

You will need to do the following:

- put `otawa/bin` in your path
- put `otawa/lib` in the `LD_LIBRARY_PATH` variable

### Compiling the plug-ins ###

Type make, all plugins are compiled. 




