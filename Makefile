all :
	$(MAKE) -C initialize_RMB_LMB
	$(MAKE) -C cache_block_id
	$(MAKE) -C UCB_local
	$(MAKE) -C ucb_otawa
	$(MAKE) -C explo
	$(MAKE) -C application

cleanall :
	$(MAKE) cleanall -C initialize_RMB_LMB
	$(MAKE) clean -C cache_block_id
	$(MAKE) clean -C UCB_local
	$(MAKE) clean -C ucb_otawa
	$(MAKE) clean -C explo
	$(MAKE) clean -C application
