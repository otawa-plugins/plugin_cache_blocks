#include <otawa/app/Application.h>
#include <otawa/cfg/features.h>
#include <otawa/script/Script.h>
#include <otawa/prog/WorkSpace.h>
#include <otawa/proc/DynProcessor.h>
#include <otawa/cfg/Dominance.h>
#include <otawa/proc/DynFeature.h>

//includes pour l'affichage du CFG
#include <otawa/display/CFGOutput.h>
#include <elm/io/OutFileStream.h>
#include "include/UCB_local.h"

using namespace otawa; //comme import
using namespace otawa::UCB_local;

int main(int argc, char **argv) {
	if (argc != 2) {
		fprintf(stderr, "usage: %s <binary> ", argv[0]);
	exit(1);
	}
	WorkSpace *ws = NULL;
	PropList conf;
	Manager manager;
	NO_SYSTEM(conf) = true;
	TASK_ENTRY(conf) = "main";
	VERBOSE(conf) = true;
 
	CACHE_CONFIG_PATH(conf) = "./cache.xml";
	StringBuffer buf;
	buf << argv[1] << "-decomp.c";
	elm::io::OutFileStream s(buf.toString());
	elm::io::Output out(s);
	conf.print(out);
	ws = manager.load(argv[1], conf);

	ws->require(DynFeature("otawa::UCB_local::UCB_LOCAL_FEATURE"), conf);

	const CFGCollection *coll = INVOLVED_CFGS(ws);

	for (CFGCollection::Iter cfg_iter(*coll); cfg_iter(); cfg_iter++)
	  {
	    for (CFG::BlockIter block_iter = cfg_iter->blocks(); block_iter(); block_iter++)
	      {
		if (block_iter->isBasic())
		  {
		    BasicBlock *bb = block_iter->toBasic();
		    Mathset::Multiset<int> ucb_in = UCB_LOCAL_IN(bb);
		    Mathset::Multiset<int> ucb_out = UCB_LOCAL_OUT(bb) ;
		    
		    std::cout << "-------------------------" << std::endl;
		    std::cout << "BLOCK:" << bb->id() << std::endl;
		    ucb_in.print_vector();
		    ucb_out.print_vector();
		    std::cout << "-------------------------" << std::endl;
		  }
	      }
	  }
}
