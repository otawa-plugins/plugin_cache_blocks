#ifndef UCB_LOCAL_H
#define UCB_LOCAL_H
#include <typeinfo>
#include <vector>
#include <algorithm>
#include <assert.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <functional>
#include <multiset.h>

#include <otawa/cfg.h>
#include <otawa/cfg/features.h>
#include <otawa/proc/ProcessorPlugin.h>
#include <otawa/cfg/Dominance.h>
#include <otawa/otawa.h>
namespace otawa { namespace UCB_local {
class UCB_localProcessor : public Processor {
public:
	static p::declare reg;
	explicit UCB_localProcessor(p::declare &r = reg);

protected:
	void processWorkSpace(WorkSpace * /*ws*/) override;
	void configure(const PropList &props) override;
};

// declaration feature
extern p::feature UCB_LOCAL_FEATURE;

//declaration propriete 

    extern Identifier<Mathset::Multiset<int>> UCB_LOCAL_IN;
    extern Identifier<Mathset::Multiset<int>> UCB_LOCAL_OUT;
    extern Identifier<Mathset::Multiset<int>> TOTAL_UCB;
    
  } }
#endif
