
#include "../initialize_RMB_LMB/include/initialize_RMB_LMB.h"
#include "include/UCB_local.h"


namespace otawa {
  namespace UCB_local {

    // Declaration du plugin

    class Plugin : public ProcessorPlugin {
    public:
      Plugin() : ProcessorPlugin("otawa::UCB_local", Version(1, 0, 0), OTAWA_PROC_VERSION) {}
    };

    otawa::UCB_local::Plugin UCB_local_plugin;
    ELM_PLUGIN(UCB_local_plugin, OTAWA_PROC_HOOK);

    p::declare UCB_localProcessor::reg = p::init("otawa::UCB_local::UCB_localProcessor", Version(1, 0, 0))
      .require(COLLECTED_CFG_FEATURE)
      .require(LOOP_INFO_FEATURE)
      .require(otawa::initialize_RMB_LMB::INITIALIZE_RMB_LMB_FEATURE)
      .provide(UCB_LOCAL_FEATURE);
    // definition feature
    p::feature UCB_LOCAL_FEATURE("otawa::UCB_local::UCB_LOCAL_FEATURE", new Maker<UCB_localProcessor>());

    UCB_localProcessor::UCB_localProcessor(p::declare &r) : Processor(r) {}

    void UCB_localProcessor::configure(const PropList &props) {
      Processor::configure(props);

    }


    Mathset::Multiset<int> compute_ucb(
                                       Mathset::Multiset<std::vector<Address>> *rmb,
                                       Mathset::Multiset<std::vector<Address>> *lmb,
                                       int nb_index, int nb_way) {
      Mathset::Multiset<int> tmp;

      for(int i = 0; i < nb_index; i++) {
        std::vector<std::vector<Address>> true_rmb_vect = rmb[i].get_set();
        std::vector<std::vector<Address>> lmb_vect = lmb[i].get_set();
        int nb_hit_max = 0;
        for(int j = 0; j < lmb_vect.size(); j++) {
          // for each different lmb set
          if(lmb_vect[j].size() != 0) {
            for(int k = 0; k < true_rmb_vect.size(); k++) {
              std::vector<Address> rmb_vect = true_rmb_vect[k];
              if(rmb_vect.size() != 0) {
                // for each different rmb set
                int nb_hit = 0;

                for(int l = 0; l < lmb_vect[j].size(); l++) {

                  bool not_found = true;

                  for(int m = 0; m < rmb_vect.size(); m++) {
                    if(lmb_vect[j][l] == rmb_vect[m]) {
                      nb_hit++;
                      not_found = false;
                      rmb_vect.erase(rmb_vect.begin()+m);
                      rmb_vect.push_back(lmb_vect[j][l]);
                      break;
                    }
                  } // m

                  if(not_found) {
                    rmb_vect.push_back(lmb_vect[j][l]);
                    while(rmb_vect.size() > nb_way) {
                      rmb_vect.erase(rmb_vect.begin());
                    }
                  }

                } //for l

                if(nb_hit > nb_hit_max) {
                  nb_hit_max = nb_hit;
                }

              } //if size > 0

            } // each different rmb set
          } //if size > 0
        } // each different lmb set

        struct Mathset::Element<int> elem;
        elem.counter = nb_hit_max;
        elem.elem = i;

        tmp.add_elem_struct(elem);

      } // for each index

      return tmp;

    }

    void UCB_localProcessor::processWorkSpace(WorkSpace *ws) {
      const CFGCollection *coll = INVOLVED_CFGS(ws);

      for (CFGCollection::Iter cfg_iter(*coll); cfg_iter(); cfg_iter++)
        {
          for (CFG::BlockIter block_iter = cfg_iter->blocks(); block_iter(); block_iter++)
            {
              if (block_iter->isBasic())
                {
                  BasicBlock *bb = block_iter->toBasic();
                  initialize_RMB_LMB::Memory_block *mb = initialize_RMB_LMB::MB_STRUCT(bb);
                  int tmp = mb->block_id;
                  UCB_LOCAL_IN(bb) = compute_ucb(mb->rmb_in, mb->lmb_in, mb->nb_index, mb->nb_way);
                  UCB_LOCAL_OUT(bb) = compute_ucb(mb->rmb_out, mb->lmb_out, mb->nb_index, mb->nb_way);

                }
            }
        }

      // COMPUTATION OF THE TOTAL UCB MULTISET

      Mathset::Multiset<int> total_ucb;
      for (CFGCollection::Iter cfg_iter(*coll); cfg_iter(); cfg_iter++)
        {
          for (CFG::BlockIter block_iter = cfg_iter->blocks(); block_iter(); block_iter++)
            {
              if (block_iter->isBasic())
                {
                  BasicBlock *bb = block_iter->toBasic();
                  initialize_RMB_LMB::Memory_block *mb = initialize_RMB_LMB::MB_STRUCT(bb);
                  int tmp = mb->block_id;
                  Mathset::Multiset<int> tmp_ucb_in = UCB_LOCAL_IN(bb);
                  Mathset::Multiset<int> tmp_ucb_out = UCB_LOCAL_OUT(bb);
                  total_ucb = math_multiset_fusion(total_ucb, tmp_ucb_in);
                  total_ucb = math_multiset_fusion(total_ucb, tmp_ucb_out);
                }
            }
        }
      TOTAL_UCB(ws) = total_ucb;
    }




    // definition propriete

    Identifier<Mathset::Multiset<int>> UCB_LOCAL_IN("otawa::UCB_local::UCB_LOCAL_IN");
    Identifier<Mathset::Multiset<int>> UCB_LOCAL_OUT("otawa::UCB_local::UCB_LOCAL_OUT");
    Identifier<Mathset::Multiset<int>> TOTAL_UCB("otawa::UCB_local::TOTAL_UCB");
  }
}
