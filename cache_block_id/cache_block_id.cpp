#include "include/cache_block_id.h"
#include <otawa/hard/CacheConfiguration.h>
#include <otawa/cache/features.h>

#include <otawa/cfg/CFG.h>
#include <otawa/cache/features.h>
#include <otawa/cache/cat2/CAT2Builder.h>
#include <otawa/cache/LBlockBuilder.h>

#include <otawa/ipet/features.h>
#include <otawa/ipet/FlowFactLoader.h>

#include <otawa/flowfact/features.h>


namespace otawa {
  namespace cache_block_id {
    // Declaration du plugin
    class Plugin : public ProcessorPlugin { 
    public:
      Plugin() : ProcessorPlugin("otawa::cache_block_id", Version(1, 0, 0), OTAWA_PROC_VERSION) {}
    };

    otawa::cache_block_id::Plugin cache_block_id_plugin;
    ELM_PLUGIN(cache_block_id_plugin, OTAWA_PROC_HOOK);

    p::declare Cache_block_idProcessor::reg = p::init("otawa::cache_block_id::Cache_block_idProcessor", Version(1, 0, 0))
      .require(COLLECTED_CFG_FEATURE)
      .require(LOOP_INFO_FEATURE)
      .require(ICACHE_CATEGORY2_FEATURE)
      .require(ipet::FLOW_FACTS_FEATURE)
      .require(MKFF_PRESERVATION_FEATURE)
      .require(cache::COLLECTED_LBLOCKS_FEATURE)
      .require(hard::CACHE_CONFIGURATION_FEATURE)
      .provide(CACHE_BLOCK_ID_FEATURE);
        
    // definition feature
    p::feature CACHE_BLOCK_ID_FEATURE("otawa::cache_block_id::CACHE_BLOCK_ID_FEATURE", new Maker<Cache_block_idProcessor>());

    Cache_block_idProcessor::Cache_block_idProcessor(p::declare &r) : Processor(r) {}
        
    void Cache_block_idProcessor::configure(const PropList &props)
    {
      Processor::configure(props);
    }

    
		
    void Cache_block_idProcessor::processWorkSpace(WorkSpace *ws) {
      const otawa::hard::CacheConfiguration *cf = otawa::hard::CACHE_CONFIGURATION_FEATURE.get(ws);
      const otawa::hard::Cache* cache = cf->instCache();
      const CFGCollection *coll = INVOLVED_CFGS(ws);
      for(CFGCollection::Iter cfg_iter(*coll); cfg_iter(); cfg_iter++) {
	for(CFG::BlockIter block_iter = cfg_iter->blocks(); block_iter(); block_iter++) {
	  if(block_iter->isBasic()) {
	    BasicBlock *bb = block_iter->toBasic();
	    AllocArray<LBlock*> *lblock_array = BB_LBLOCKS(bb);
	    for(int i = 0; i < lblock_array->count(); i++) {
	      LBlock *lb = (*lblock_array)[i];
	      otawa::Address cb_id = cache->round(lb->address());
	      CB_ID(lb) = cb_id;
	    }
	  }
	}
      }
    }



    // definition propriete 

    Identifier<Address> CB_ID("otawa::cache_block_id::CB_ID");


  }
}
