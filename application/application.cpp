#include <otawa/app/Application.h>
#include <otawa/cfg/features.h>
#include <otawa/script/Script.h>
#include <otawa/prog/WorkSpace.h>
#include <otawa/proc/DynProcessor.h>
#include <otawa/cfg/Dominance.h>
#include <otawa/proc/DynFeature.h>
#include <otawa/ipet/features.h>
#include <otawa/ipet/FlowFactLoader.h>
#include <otawa/flowfact/features.h>
#include <fstream>

//includes pour l'affichage du CFG
#include <otawa/display/CFGOutput.h>
#include <otawa/cache/features.h>
#include <otawa/cache/cat2/CAT2Builder.h>
// edge time processors
#include <otawa/etime/features.h>

#include <elm/io/OutFileStream.h>
#include <otawa/flowfact/features.h>
//#include <multiset.h>
#include "../classic_ecb/include/classic_ecb.h"
#include "../ucb_otawa/include/ucb_otawa.h"
#include "application.h"

using namespace otawa; //comme import
using namespace otawa::classic_ecb;
using namespace otawa::ucb_otawa;
using namespace Mathset;



int main(int argc, char **argv) {
  if (argc < 5) {
    fprintf(stderr, "usage: %s <binary> <cache> <flow facts> <sortie>\n", argv[0]);
    exit(1);
  }

  // //----------------------------- TRIVIAL
  WorkSpace *ws_trivial = NULL;
  PropList conf_trivial;
  Manager manager_trivial;
  NO_SYSTEM(conf_trivial) = true;
  TASK_ENTRY(conf_trivial) = "main";
  VERBOSE(conf_trivial) = true;
  FLOW_FACTS_PATH(conf_trivial) = argv[3];
  
  ws_trivial = manager_trivial.load(argv[1], conf_trivial);
  ws_trivial->run("otawa::Virtualizer", conf_trivial);

  ws_trivial->require(otawa::ipet::WCET_FEATURE, conf_trivial);
  long wcet_trivial = ipet::WCET(ws_trivial);
  
  // //-------------------------------------------------------------
  WorkSpace *ws = NULL;
  PropList conf;
  Manager manager;
  NO_SYSTEM(conf) = true;
  TASK_ENTRY(conf) = "main";
  VERBOSE(conf) = true;

  
        
  CACHE_CONFIG_PATH(conf) = argv[2];
  FLOW_FACTS_PATH(conf) = argv[3];
  PROCESSOR_PATH(conf)  = "/home/fabien/cristal/otawa-plugins/application/hardware/pipeline/lpc2138.xml";
  MEMORY_PATH(conf)  = "/home/fabien/cristal/otawa-plugins/application/hardware/memory/lpc2138.xml";
  otawa::etime::RECORD_TIME(conf) = true;

  // Platform description
  //CACHE_CONFIG_PATH(conf) = "/home/fabien/cristal/otawa-plugins/application/xmc4500/cache.xml";
  //PROCESSOR_PATH(conf)  = "/home/fabien/cristal/otawa-plugins/application/xmc4500/pipeline.xml";
  
  ws = manager.load(argv[1], conf);
  ws->run("otawa::Virtualizer", conf);
  ws->require(otawa::ICACHE_CONSTRAINT2_FEATURE, conf);
  //ws->require(otawa::ICACHE_ONLY_CONSTRAINT2_FEATURE, conf);
  ws->require(otawa::etime::EDGE_TIME_FEATURE, conf);
  ws->require(otawa::ipet::WCET_FEATURE, conf);
   
  ws->require(DynFeature("otawa::ucb_otawa::UCB_OTAWA_FEATURE"), conf);
  ws->require(DynFeature("otawa::classic_ecb::CLASSIC_ECB_PROCESSOR"), conf);
  //ws->require(otawa::ipet::ILP_SYSTEM_FEATURE, conf);
  
 
  long wcet = ipet::WCET(ws) - wcet_trivial;
  Multiset<int> ucbs = TOTAL_UCB(ws);
  Multiset<int> *ecbs = ECBS(ws);

  if(wcet > 1000000) {
    return 0;
  }

  std::ofstream f(argv[4], std::ofstream::app);
  
   
  f <<"(" << argv[1] << "," << wcet << ", 10, 10 ," << std::endl << "{";
  ecbs->to_string(f);

   
  f << "}," << std::endl << "{";
  ucbs.to_string(f);
  f << "}," << std::endl << "{";

  
  const CFGCollection *coll = INVOLVED_CFGS(ws);

  Mathset::Multiset<Mathset::Multiset<int>> ucb_list = UCB_OTAWA(ws);
  for(int i = 0; i < ucb_list.size(); i++) {
    f << "{";
    ucb_list[i].to_string(f);
    f << "}";
  }

  f << "})" << endl;
	
  f.close();
  const otawa::hard::CacheConfiguration *cf = otawa::hard::CACHE_CONFIGURATION_FEATURE.get(ws); 
  const otawa::hard::Cache* cache = cf->instCache();

  int nb_index = cache->rowCount();
  int nb_way = cache->wayCount();
  int miss_pe = cache->missPenalty();

  cout << "index:" << nb_index << " way:" << nb_way << " misspenalty:" << miss_pe << endl;
  
}

