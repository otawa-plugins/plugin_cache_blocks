#include <multiset.h>

template <typename T>
std::vector<Mathset::Multiset<T>*> compr_multiset(std::vector<Mathset::Multiset<T>*> v) {
	std::vector<Mathset::Multiset<T>*> result;
	for(auto iter_v = v.begin(); iter_v != v.end(); iter_v++) {
		bool important_set = true;
		for(auto tmp = iter_v + 1; tmp < v.end(); tmp++) {
			if((*tmp)->contain(**iter_v)) {
				important_set = false;
			} 
		}
		if(important_set) {
			result.push_back(*iter_v);
		}
	}
	return result;
}
