#ifndef LOOP_BOUND_H
#define LOOP_BOUND_H
#include <typeinfo>
#include <vector>
#include <algorithm>
#include <assert.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <functional>

#include <otawa/cfg.h>
#include <otawa/cfg/features.h>
#include <otawa/proc/ProcessorPlugin.h>
#include <otawa/cfg/Dominance.h>
#include <otawa/otawa.h>
namespace otawa { namespace loop_bound {
class Loop_boundProcessor : public Processor {
public:
	static p::declare reg;
	explicit Loop_boundProcessor(p::declare &r = reg);

protected:
	void processWorkSpace(WorkSpace * /*ws*/) override;
	void configure(const PropList &props) override;
};

// declaration feature
extern p::feature LOOP_BOUND_FEATURE;

//declaration propriete 

    extern Identifier<int> TOTAL_BOUND;
    extern Identifier<int> MAX_BOUND;
    extern Identifier<bool> IN_LOOP;
} }
#endif
