#include <otawa/app/Application.h>
#include <otawa/cfg/features.h>
#include <otawa/script/Script.h>
#include <otawa/prog/WorkSpace.h>
#include <otawa/proc/DynProcessor.h>
#include <otawa/cfg/Dominance.h>
#include <otawa/proc/DynFeature.h>

//includes pour l'affichage du CFG
#include <otawa/display/CFGOutput.h>
#include <elm/io/OutFileStream.h>
#include <otawa/flowfact/features.h>

#include "include/loop_bound.h"

using namespace otawa; //comme import
using namespace otawa::loop_bound;

int main(int argc, char **argv) {
	if (argc != 3) {
		fprintf(stderr, "usage: %s <binary> <flow fact> ", argv[0]);
	exit(1);
	}
	WorkSpace *ws = NULL;
	PropList conf;
	Manager manager;
	NO_SYSTEM(conf) = true;
	TASK_ENTRY(conf) = "main";
	VERBOSE(conf) = true;
 
	CACHE_CONFIG_PATH(conf) = "./cache.xml";
	FLOW_FACTS_PATH(conf) = argv[2];
	StringBuffer buf;
	buf << argv[1] << "-decomp.c";
	elm::io::OutFileStream s(buf.toString());
	elm::io::Output out(s);
	conf.print(out);
	ws = manager.load(argv[1], conf);

	ws->require(DynFeature("otawa::loop_bound::LOOP_BOUND_FEATURE"), conf);
}
