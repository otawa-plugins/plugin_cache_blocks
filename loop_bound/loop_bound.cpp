#include "include/loop_bound.h"
#include <otawa/cfg/Loop.h>
#include <otawa/ipet/FlowFactLoader.h>
#include <otawa/flowfact/features.h>

namespace otawa {
  namespace loop_bound {

    // Declaration du plugin

    class Plugin : public ProcessorPlugin { 
    public:
      Plugin() : ProcessorPlugin("otawa::loop_bound", Version(1, 0, 0), OTAWA_PROC_VERSION) {}
    };

    otawa::loop_bound::Plugin loop_bound_plugin;
    ELM_PLUGIN(loop_bound_plugin, OTAWA_PROC_HOOK);

    p::declare Loop_boundProcessor::reg = p::init("otawa::loop_bound::Loop_boundProcessor", Version(1, 0, 0))
      .require(COLLECTED_CFG_FEATURE)
      .require(LOOP_INFO_FEATURE)
      .require(MKFF_PRESERVATION_FEATURE)
      .require(FLOW_FACTS_FEATURE)
      .provide(LOOP_BOUND_FEATURE);
    // definition feature
    p::feature LOOP_BOUND_FEATURE("otawa::loop_bound::LOOP_BOUND_FEATURE", new Maker<Loop_boundProcessor>());

    Loop_boundProcessor::Loop_boundProcessor(p::declare &r) : Processor(r) {}

    void Loop_boundProcessor::configure(const PropList &props) {
      Processor::configure(props);

    }


    

    int sum_iteration_caller(CFG* cfg, bool &in_loop) {
      int iteration = 0;
      elm::List<otawa::SynthBlock*> list = cfg->callers();
      for(int i = 0; i < list.count(); i++) {
	SynthBlock * sb = list[i];
	Loop* l = Loop::of(sb);
	Block* header = l->header();
	if(header->isBasic()) {
	  in_loop = true;
	  iteration += TOTAL_ITERATION(header->toBasic()->first());
	} else {
	  iteration += sum_iteration_caller(header->cfg(), in_loop);
	}
      }
      if(iteration == 0) {
	return 1;
      }
      return iteration;
    }
    
    void explo_collection(WorkSpace *ws) {
      const CFGCollection *coll = INVOLVED_CFGS(ws);
      for (CFGCollection::Iter cfg_iter(*coll); cfg_iter(); cfg_iter++) {
	for (CFG::BlockIter block_iter = cfg_iter->blocks(); block_iter(); block_iter++) {
	  Block* b = *block_iter;
	  Loop* l = Loop::of(b);
	  int iteration = 0;
	  int iteration_max = 0;
	  bool in_loop = false;
	  if(l != NULL) {
	    Block* header = l->header();
	  
	    if(header->isBasic()) {
	      in_loop = true;
	      iteration = TOTAL_ITERATION(header->toBasic()->first());
	      iteration_max = MAX_ITERATION(header->toBasic()->first());
	    } else {
	      iteration = sum_iteration_caller(header->cfg(), in_loop);
	      iteration_max = 1;
	    }
	  
	  } else {
	    iteration = 1;
	    iteration_max = 1;
	  }
	  if(iteration == 0) {
	    iteration =  1;
	  }
	  IN_LOOP(b) = in_loop;
	  TOTAL_BOUND(b) = iteration;
	  MAX_BOUND(b) = iteration_max;
	}
      }
    }
    
    void Loop_boundProcessor::processWorkSpace(WorkSpace *ws) {
      
      explo_collection(ws);
    }



    // definition propriete 
    
    Identifier<int> TOTAL_BOUND("otawa::loop_bound::TOTAL_BOUND");
    Identifier<int> MAX_BOUND("otawa::loop_bound::MAX_BOUND");
    Identifier<bool> IN_LOOP("otawa::loop_bound::IN_LOOP");

  }
}
