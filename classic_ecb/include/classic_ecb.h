#ifndef CLASSIC_ECB
#define CLASSIC_ECB 1
#include <typeinfo>
#include <vector>
#include <algorithm>
#include <assert.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <functional>

#include <otawa/base.h>
#include <otawa/cfg.h>
#include <otawa/cfg/features.h>
#include <otawa/proc/ProcessorPlugin.h>
#include <otawa/cfg/Dominance.h>
#include <otawa/otawa.h>

#include <otawa/cache/LBlock.h>
#include <otawa/cache/features.h>
#include "multiset.h"

namespace otawa { namespace classic_ecb {

class Classic_EcbProcessor : public Processor {
public:
	static p::declare reg;
	explicit Classic_EcbProcessor(p::declare &r = reg);

protected:
	void processWorkSpace(WorkSpace * /*ws*/) override;
	void configure(const PropList &props) override;
};

// declaration feature
extern p::feature CLASSIC_ECB_PROCESSOR;

//declaration propriete CFTRee

	extern Identifier<Mathset::Multiset<int>*> ECBS;
} }

#endif
