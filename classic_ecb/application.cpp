#include <otawa/app/Application.h>
#include <otawa/cfg/features.h>
#include <otawa/script/Script.h>
#include <otawa/prog/WorkSpace.h>
#include <otawa/proc/DynProcessor.h>
#include <otawa/cfg/Dominance.h>
#include <otawa/proc/DynFeature.h>

//includes pour l'affichage du CFG
#include <otawa/display/CFGOutput.h>
#include <elm/io/OutFileStream.h>
#include <otawa/flowfact/features.h>
#include "include/classic_ecb.h"

using namespace otawa; //comme import
using namespace otawa::classic_ecb;
using namespace Mathset;


int main(int argc, char **argv) {
	if (argc != 2 && argc != 3) {
		fprintf(stderr, "usage: %s <binary> <flow fact file> \n", argv[0]);
		exit(1);
	}
	WorkSpace *ws = NULL;
	PropList conf;
	Manager manager;
	NO_SYSTEM(conf) = true;
	TASK_ENTRY(conf) = "main";
	VERBOSE(conf) = true;
	std::cout << "----------lecture fichier cache--------------" << std::endl;
	CACHE_CONFIG_PATH(conf) = "./cache.xml";
	if(argc == 3) {
	    FLOW_FACTS_PATH(conf) = argv[2];
	}
	std::cout << "----------fin traitement configuration cache--------------" << std::endl;
	StringBuffer buf;
	buf << argv[1] << "-decomp.c";
	elm::io::OutFileStream s(buf.toString());
	elm::io::Output out(s);
	conf.print(out);

	std::cout << "----------chargement tâche-------------" << std::endl;
	ws = manager.load(argv[1], conf);

	std::cout << "----------traitement de la tâche------------" << std::endl;
	ws->require(DynFeature("otawa::classic_ecb::CLASSIC_ECB_PROCESSOR"), conf);
	std::cout << "---------fin de traitement de la tâche----------" << std::endl;
	//const CFGCollection *coll = INVOLVED_CFGS(ws);
	/*StringBuffer buf;
	buf << argv[1] << "-decomp.c";
	elm::io::OutFileStream s(buf.toString());
	elm::io::Output out(s);
	*/
	// for (CFGCollection::Iter iter(*coll); iter; iter ++) {
	// 	CFG *currentCFG = *iter;
		/*StringBuffer buf;
		buf << argv[1] << "-" << currentCFG->label() << "-cftree.dot";*/
	
	//std::vector<Element<Address>> ecbs = ECBS(ws)->get_multiset();
	Multiset<int> *ecbs = ECBS(ws);
	ecbs->print_vector();
        
	// for(auto iter_add = ecbs.begin(); iter_add != ecbs.end(); iter_add++) {
	// 		std::cout << std::endl << "page: " << iter_add->elem.page() << " offset: " << iter_add->elem.offset() << std::endl;
	// }
		
	//}


}
