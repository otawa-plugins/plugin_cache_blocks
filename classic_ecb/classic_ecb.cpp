//#define __NO_LOOP_BOUND__
#include "include/classic_ecb.h"
#include <multiset.h>
#include <otawa/hard/CacheConfiguration.h>
#include <otawa/cache/features.h>

#include <otawa/cfg/CFG.h>
#include <otawa/cfg/Loop.h>

#include <otawa/cache/features.h>
#include <otawa/cache/cat2/CAT2Builder.h>
#include <otawa/cache/LBlockBuilder.h>

#include <otawa/ipet/features.h>
#include <otawa/ipet/FlowFactLoader.h>

#include <otawa/flowfact/features.h>

#include "../loop_bound/include/loop_bound.h"

using namespace Mathset;
namespace otawa {
  namespace classic_ecb {

    // Declaration du plugin

    class Plugin : public ProcessorPlugin { 
    public:
      Plugin() : ProcessorPlugin("otawa::classic_ecb", Version(1, 0, 0), OTAWA_PROC_VERSION) {}
    };

    otawa::classic_ecb::Plugin classic_ecb_plugin;
    ELM_PLUGIN(classic_ecb_plugin, OTAWA_PROC_HOOK);

    p::declare Classic_EcbProcessor::reg = p::init("otawa::classic_ecb::Classic_EcbProcessor", Version(1, 0, 0))
      .require(COLLECTED_CFG_FEATURE)
      .require(LOOP_INFO_FEATURE)
      .require(ICACHE_CATEGORY2_FEATURE)
      .require(ipet::FLOW_FACTS_FEATURE)
      .require(MKFF_PRESERVATION_FEATURE)
      .require(cache::COLLECTED_LBLOCKS_FEATURE)
      .require(hard::CACHE_CONFIGURATION_FEATURE)
      .require(loop_bound::LOOP_BOUND_FEATURE)
      .provide(CLASSIC_ECB_PROCESSOR);
	
    // definition feature
    p::feature CLASSIC_ECB_PROCESSOR("otawa::classic_ecb::CLASSIC_ECB_PROCESSOR", new Maker<Classic_EcbProcessor>());

    Classic_EcbProcessor::Classic_EcbProcessor(p::declare &r) : Processor(r) {}

    void Classic_EcbProcessor::configure(const PropList &props) {
      Processor::configure(props);
    }

    
    void Classic_EcbProcessor::processWorkSpace(WorkSpace *ws) {

      const otawa::hard::CacheConfiguration *cf = otawa::hard::CACHE_CONFIGURATION_FEATURE.get(ws);
      const otawa::hard::Cache* cache = cf->instCache();
      
      Multiset<int> cache_full;
      for(int i = 0; i < cache->rowCount(); i++) {
	struct Element<int> elem;
	elem.counter = cache->wayCount();
	elem.elem = i;
	cache_full.add_elem_struct(elem);
      }
      
      Multiset<int> ecb_set;
      const CFGCollection *coll = INVOLVED_CFGS(ws);
      for(CFGCollection::Iter cfg_iter(*coll); cfg_iter(); cfg_iter++) {
	for(CFG::BlockIter block_iter = cfg_iter->blocks(); block_iter(); block_iter++) {
	  if(block_iter->isBasic()) {
	    BasicBlock *bb = block_iter->toBasic();

	    int nb_iter = loop_bound::TOTAL_BOUND(bb);
	    int nb_max_iter = loop_bound::MAX_BOUND(bb);
	    
	    AllocArray<LBlock*> *lblock_array = BB_LBLOCKS(bb);
	    
	    for(int i = 0; i < lblock_array->count(); i++) {
	      LBlock *lb = (*lblock_array)[i];
	      cache::category_t status = cache::CATEGORY(lb);
	      
	      struct Element<int> tmp_e;
	      tmp_e.elem = cache->line(lb->address());
	      // one block may erase a full cache
	      tmp_e.counter = cache->wayCount();
	      ecb_set.add_elem_struct(tmp_e);
		
	      /*
	      switch(status) {
	      case ALWAYS_HIT:
		break;

	      case FIRST_MISS:
		#ifndef __NO_LOOP_BOUND__
		tmp_e.counter = max(nb_iter/nb_max_iter, 1); 
		#else
		if(loop_bound::IN_LOOP(bb)) {
		  tmp_e.counter = cache->wayCount();
		} else {
		  tmp_e.counter = 1;
		}
		#endif
		ecb_set.add_elem_struct(tmp_e);
		break;
		
	      case FIRST_HIT:
		#ifndef __NO_LOOP_BOUND__
		tmp_e.counter = max(nb_iter - (nb_iter/nb_max_iter), 1);
		#else
		if(loop_bound::IN_LOOP(bb)) {
		  tmp_e.counter = cache->wayCount();
		} else {
		  tmp_e.counter = 1;
		}
		#endif
		ecb_set.add_elem_struct(tmp_e);
		break;

	      default:
		#ifndef __NO_LOOP_BOUND__
		tmp_e.counter = max(nb_iter, 1);
		#else
		if(loop_bound::IN_LOOP(bb)) {
		  tmp_e.counter = cache->wayCount();
		} else {
		  tmp_e.counter = 1;
		}
		#endif
		ecb_set.add_elem_struct(tmp_e);
		break;
	      }	
	      */	    
	    }
	  } 
	}
      }

      Multiset<int> tmp_result = math_multiset_intersection(ecb_set, cache_full);
      Multiset<int>* result = new Multiset<int>;
      std::vector<struct Element<int>> elements = tmp_result.get_multiset();
      for(int i = 0; i < elements.size(); i++) {
	result->add_elem_struct(elements[i]);
      }
      ECBS(ws) = result;
    }
	    
	    

    // definition propriete 
	
    Identifier<Mathset::Multiset<int>*> ECBS("otawa::ecb_extractor::ECBS");
  }
}
