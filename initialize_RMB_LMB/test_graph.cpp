#define CATCH_CONFIG_MAIN
#include "catch.hpp"

#include <otawa/app/Application.h>
#include <otawa/cfg/features.h>
#include <otawa/script/Script.h>
#include <otawa/prog/WorkSpace.h>
#include <otawa/proc/DynProcessor.h>
#include <otawa/cfg/Dominance.h>
#include <otawa/proc/DynFeature.h>

#include <iostream>
#include <fstream>

//includes pour l'affichage du CFG
#include <otawa/display/CFGOutput.h>
#include <elm/io/OutFileStream.h>

#include "include/initialize_RMB_LMB.h"
#include "include/type.h"

using namespace otawa; //comme import
using namespace otawa::initialize_RMB_LMB;

TEST_CASE("condition","[graph]") {

  WorkSpace *ws = NULL;
  PropList conf;
  Manager manager;
  NO_SYSTEM(conf) = true;
  TASK_ENTRY(conf) = "main";
  VERBOSE(conf) = true;
 
  CACHE_CONFIG_PATH(conf) = "./cache.xml";
  StringBuffer buf;
  buf << "test_condition-decomp.c";
  elm::io::OutFileStream s(buf.toString());
  elm::io::Output out(s);
  conf.print(out);
  ws = manager.load("condition", conf);

  ws->require(DynFeature("otawa::initialize_RMB_LMB::INITIALIZE_RMB_LMB_FEATURE"), conf);

 
  
  const CFGCollection *coll = INVOLVED_CFGS(ws);
  for(CFGCollection::Iter cfg_iter(*coll); cfg_iter(); cfg_iter++) {
    for(CFG::BlockIter block_iter = cfg_iter->blocks(); block_iter(); block_iter++) {
      if(block_iter->isBasic()) {
	BasicBlock *bb = block_iter->toBasic();
	struct Memory_block *mb = MB_STRUCT(bb);
	if(mb->block_id == 1) {
	  REQUIRE(mb->suc.size() == 2);
	  REQUIRE(mb->suc[0]->block_id == 2);
	  REQUIRE(mb->suc[1]->block_id == 3);
	  REQUIRE(mb->pred.size() == 0);
	}
	if(mb->block_id == 2) {
	  REQUIRE(mb->pred.size() == 1);
	  REQUIRE(mb->pred[0]->block_id == 1);
	  REQUIRE(mb->suc.size() == 1);
	  REQUIRE(mb->suc[0]->block_id == 4);
	}
	if(mb->block_id == 3) {
	  REQUIRE(mb->pred.size() == 1);
	  REQUIRE(mb->pred[0]->block_id == 1);
	  REQUIRE(mb->suc.size() == 1);
	  REQUIRE(mb->suc[0]->block_id == 4);
	}
	if(mb->block_id == 4) {
	  REQUIRE(mb->pred.size() == 2);
	  REQUIRE(mb->pred[0]->block_id == 2);
	  REQUIRE(mb->pred[1]->block_id == 3);
	  REQUIRE(mb->suc.size() == 0);
	}
      }
    }
  }

}


TEST_CASE("loop","[graph]") {

  WorkSpace *ws = NULL;
  PropList conf;
  Manager manager;
  NO_SYSTEM(conf) = true;
  TASK_ENTRY(conf) = "main";
  VERBOSE(conf) = true;
 
  CACHE_CONFIG_PATH(conf) = "./cache.xml";
  StringBuffer buf;
  buf << "test_loop-decomp.c";
  elm::io::OutFileStream s(buf.toString());
  elm::io::Output out(s);
  conf.print(out);
  ws = manager.load("loop", conf);

  ws->require(DynFeature("otawa::initialize_RMB_LMB::INITIALIZE_RMB_LMB_FEATURE"), conf);

 
  
  const CFGCollection *coll = INVOLVED_CFGS(ws);
  for(CFGCollection::Iter cfg_iter(*coll); cfg_iter(); cfg_iter++) {
    for(CFG::BlockIter block_iter = cfg_iter->blocks(); block_iter(); block_iter++) {
      if(block_iter->isBasic()) {
	BasicBlock *bb = block_iter->toBasic();
	struct Memory_block *mb = MB_STRUCT(bb);
	if(mb->block_id == 1) {
	  REQUIRE(mb->suc.size() == 1);
	  REQUIRE(mb->suc[0]->block_id == 2);
	  REQUIRE(mb->pred.size() == 0);
	}
	if(mb->block_id == 2) {
	  REQUIRE(mb->pred.size() == 2);
	  REQUIRE(mb->pred[0]->block_id == 1);
	  REQUIRE(mb->pred[1]->block_id == 3);
	  REQUIRE(mb->suc.size() == 2);
	  REQUIRE(mb->suc[0]->block_id == 3);
	  REQUIRE(mb->suc[1]->block_id == 4);
	}
	if(mb->block_id == 3) {
	  REQUIRE(mb->pred.size() == 1);
	  REQUIRE(mb->pred[0]->block_id == 2);
	  REQUIRE(mb->suc.size() == 1);
	  REQUIRE(mb->suc[0]->block_id == 2);
	}
	if(mb->block_id == 4) {
	  REQUIRE(mb->pred.size() == 1);
	  REQUIRE(mb->pred[0]->block_id == 2);
	  REQUIRE(mb->suc.size() == 0);
	}
      }
    }
  }

}

TEST_CASE("one_function","[graph]") {

  WorkSpace *ws = NULL;
  PropList conf;
  Manager manager;
  NO_SYSTEM(conf) = true;
  TASK_ENTRY(conf) = "main";
  VERBOSE(conf) = true;
 
  CACHE_CONFIG_PATH(conf) = "./cache.xml";
  StringBuffer buf;
  buf << "test_one_function-decomp.c";
  elm::io::OutFileStream s(buf.toString());
  elm::io::Output out(s);
  conf.print(out);
  ws = manager.load("one_function", conf);

  ws->require(DynFeature("otawa::initialize_RMB_LMB::INITIALIZE_RMB_LMB_FEATURE"), conf);

 
  
  const CFGCollection *coll = INVOLVED_CFGS(ws);
  for(CFGCollection::Iter cfg_iter(*coll); cfg_iter(); cfg_iter++) {
    for(CFG::BlockIter block_iter = cfg_iter->blocks(); block_iter(); block_iter++) {
      if(block_iter->isBasic()) {
	BasicBlock *bb = block_iter->toBasic();
	struct Memory_block *mb = MB_STRUCT(bb);
	if(mb->block_id == 1) {
	  REQUIRE(mb->suc.size() == 1);
	  REQUIRE(mb->suc[0]->block_id == 8);
	  REQUIRE(mb->pred.size() == 0);
	}
	if(mb->block_id == 2) {
	  REQUIRE(mb->pred.size() == 1);
	  REQUIRE(mb->pred[0]->block_id == 8);
	  REQUIRE(mb->suc.size() == 1);
	  REQUIRE(mb->suc[0]->block_id == 8);
	}
	if(mb->block_id == 8) {
	  REQUIRE(mb->pred.size() == 2);
	  REQUIRE(mb->pred[0]->block_id == 1);
	  REQUIRE(mb->pred[1]->block_id == 2);
	  REQUIRE(mb->suc.size() == 2);
	  REQUIRE(mb->suc[0]->block_id == 2);
	  REQUIRE(mb->suc[1]->block_id == 3);
	}
	if(mb->block_id == 3) {
	  REQUIRE(mb->pred.size() == 1);
	  REQUIRE(mb->pred[0]->block_id == 8);
	  REQUIRE(mb->suc.size() == 0);
	}
      }
    }
  }

}
