//#define CATCH_CONFIG_MAIN
#include "catch.hpp"

#include <otawa/app/Application.h>
#include <otawa/cfg/features.h>
#include <otawa/script/Script.h>
#include <otawa/prog/WorkSpace.h>
#include <otawa/proc/DynProcessor.h>
#include <otawa/cfg/Dominance.h>
#include <otawa/proc/DynFeature.h>

#include <iostream>
#include <fstream>

//includes pour l'affichage du CFG
#include <otawa/display/CFGOutput.h>
#include <elm/io/OutFileStream.h>

#include "include/initialize_RMB_LMB.h"
#include "include/type.h"

using namespace std;
using namespace otawa; //comme import
using namespace otawa::initialize_RMB_LMB;

TEST_CASE("gen","[Memory Block]") {

  WorkSpace *ws = NULL;
  PropList conf;
  Manager manager;
  NO_SYSTEM(conf) = true;
  TASK_ENTRY(conf) = "main";
  VERBOSE(conf) = true;
 
  CACHE_CONFIG_PATH(conf) = "./cache_large_code.xml";
  StringBuffer buf;
  buf << "test_condition-decomp.c";
  elm::io::OutFileStream s(buf.toString());
  elm::io::Output out(s);
  conf.print(out);
  ws = manager.load("large_loop", conf);

  ws->require(DynFeature("otawa::initialize_RMB_LMB::INITIALIZE_RMB_LMB_FEATURE"), conf);

 
  
  const CFGCollection *coll = INVOLVED_CFGS(ws);
  for(CFGCollection::Iter cfg_iter(*coll); cfg_iter(); cfg_iter++) {
    for(CFG::BlockIter block_iter = cfg_iter->blocks(); block_iter(); block_iter++) {
      if(block_iter->isBasic()) {
	BasicBlock *bb = block_iter->toBasic();
	struct Memory_block *mb = MB_STRUCT(bb);
	if(mb->block_id == 1) {
	  REQUIRE(mb->lmb_gen[0].size() == 1);
	  if(mb->lmb_gen[0].size() == 1) {
	    REQUIRE(mb->lmb_gen[0][0].offset() == 32768);
	  }
	  REQUIRE(mb->lmb_gen[1].size() == 0);
	 
	  REQUIRE(mb->rmb_gen[0].size() == 1);
	  if(mb->rmb_gen[0].size() == 1) {
	    REQUIRE(mb->rmb_gen[0][0].offset() == 32768);
	  }
	  
	  REQUIRE(mb->rmb_gen[1].size() == 0);
	}
	if(mb->block_id == 2) {
	  REQUIRE(mb->lmb_gen[0].size() == 0);
	  REQUIRE(mb->lmb_gen[1].size() == 1);
	  if(mb->lmb_gen[1].size() == 1) {
	    REQUIRE(mb->lmb_gen[1][0].offset() == 32960);
	  }

	  REQUIRE(mb->rmb_gen[0].size() == 0);
	  REQUIRE(mb->rmb_gen[1].size() == 1);
	  if(mb->rmb_gen[1].size() == 1) {
	    REQUIRE(mb->rmb_gen[1][0].offset() == 32960);
	  }
	}
	if(mb->block_id == 3) {
	  REQUIRE(mb->lmb_gen[0].size() == 2);
	  if(mb->lmb_gen[0].size() == 2) {
	    REQUIRE(mb->lmb_gen[0][0].offset() == 32768);
	    REQUIRE(mb->lmb_gen[0][1].offset() == 32896);
	  }
	  REQUIRE(mb->lmb_gen[1].size() == 2);
	  if(mb->lmb_gen[1].size() == 2) {
	    REQUIRE(mb->lmb_gen[1][0].offset() == 32832);
	    REQUIRE(mb->lmb_gen[1][1].offset() == 32960);
	  }
	  REQUIRE(mb->rmb_gen[0].size() == 2);
	  if(mb->rmb_gen[0].size() == 2) {
	    REQUIRE(mb->rmb_gen[0][0].offset() == 32768);
	    REQUIRE(mb->rmb_gen[0][1].offset() == 32896);
	  }
	  REQUIRE(mb->rmb_gen[1].size() == 2);
	  if(mb->rmb_gen[1].size() == 2) {
	    REQUIRE(mb->rmb_gen[1][0].offset() == 32832);
	    REQUIRE(mb->rmb_gen[1][1].offset() == 32960);
	  }
	}
	if(mb->block_id == 4) {
	  REQUIRE(mb->lmb_gen[0].size() == 0);
	  REQUIRE(mb->lmb_gen[1].size() == 1);
	  if(mb->lmb_gen[1].size() == 1) {
	    REQUIRE(mb->lmb_gen[1][0].offset() == 32960);
	  }

	  REQUIRE(mb->rmb_gen[0].size() == 0);
	  REQUIRE(mb->rmb_gen[1].size() == 1);
	  if(mb->rmb_gen[1].size() == 1) {
	    REQUIRE(mb->rmb_gen[1][0].offset() == 32960);
	  }
	}
      }
    }
  }

}




TEST_CASE("RMB","[Memory Block]") {

  WorkSpace *ws = NULL;
  PropList conf;
  Manager manager;
  NO_SYSTEM(conf) = true;
  TASK_ENTRY(conf) = "main";
  VERBOSE(conf) = true;
 
  CACHE_CONFIG_PATH(conf) = "./cache_large_code.xml";
  StringBuffer buf;
  buf << "test_condition-decomp.c";
  elm::io::OutFileStream s(buf.toString());
  elm::io::Output out(s);
  conf.print(out);
  ws = manager.load("large_loop", conf);

  ws->require(DynFeature("otawa::initialize_RMB_LMB::INITIALIZE_RMB_LMB_FEATURE"), conf);

 

  
  const CFGCollection *coll = INVOLVED_CFGS(ws);
  for(CFGCollection::Iter cfg_iter(*coll); cfg_iter(); cfg_iter++) {
    for(CFG::BlockIter block_iter = cfg_iter->blocks(); block_iter(); block_iter++) {
      if(block_iter->isBasic()) {
	BasicBlock *bb = block_iter->toBasic();
        
	struct Memory_block *mb = MB_STRUCT(bb);
	// Block 1
	if(mb->block_id == 1) {
	  std::vector<std::vector<Address>> RMB_IN_0 = mb->rmb_in[0];
	  std::vector<std::vector<Address>> RMB_IN_1 = mb->rmb_in[1];
	  std::vector<std::vector<Address>> RMB_OUT_0 = mb->rmb_out[0];
	  std::vector<std::vector<Address>> RMB_OUT_1 = mb->rmb_out[1];
	  // Index 0
	  REQUIRE(RMB_IN_0.size() == 0);
	  REQUIRE(RMB_OUT_0.size() == 1);
	  if(RMB_OUT_0.size() == 1) {
	    REQUIRE(RMB_OUT_0[0].size() == 1);
	    if(RMB_OUT_0[0].size() == 1) {
	      REQUIRE(RMB_OUT_0[0][0].offset() == 32768);
	    }
	  }
	  // Index 1
	  REQUIRE(RMB_IN_1.size() == 0);
	  REQUIRE(RMB_OUT_1.size() == 1);
	  if(RMB_OUT_1.size() == 1) {
	    RMB_OUT_1[0].size() == 0;
	  }
	  
	}
	// Block 2
	if(mb->block_id == 2) {
	  std::vector<std::vector<Address>> RMB_IN_0 = mb->rmb_in[0];
	  std::vector<std::vector<Address>> RMB_IN_1 = mb->rmb_in[1];
	  std::vector<std::vector<Address>> RMB_OUT_0 = mb->rmb_out[0];
	  std::vector<std::vector<Address>> RMB_OUT_1 = mb->rmb_out[1];
	  
	  // Index 0
	  REQUIRE(RMB_IN_0.size() == 2);
	  if(RMB_IN_0.size() == 2) {
	    REQUIRE(RMB_IN_0[0].size() == 1);
	    if(RMB_IN_0[0].size() == 1) {
	      REQUIRE(RMB_IN_0[0][0].offset() == 32768);
	    }
	    REQUIRE(RMB_IN_0[1].size() == 2);
	    if(RMB_IN_0[1].size() == 2) {
	      REQUIRE(RMB_IN_0[1][0].offset() == 32768);
	      REQUIRE(RMB_IN_0[1][1].offset() == 32896);
	    }
	  }
	  REQUIRE(RMB_OUT_0.size() == 2);
	  if(RMB_OUT_0.size() == 2) {
	    REQUIRE(RMB_OUT_0[0].size() == 1);
	    if(RMB_OUT_0[0].size() == 1) {
	      REQUIRE(RMB_OUT_0[0][0].offset() == 32768);
	    }
	    REQUIRE(RMB_OUT_0[1].size() == 2);
	    if(RMB_OUT_0[1].size() == 2) {
	      REQUIRE(RMB_OUT_0[1][0].offset() == 32768);
	      REQUIRE(RMB_OUT_0[1][1].offset() == 32896);
	    }
	  }
	  // Index 1
	  REQUIRE(RMB_IN_1.size() == 2);
	  if(RMB_IN_1.size() == 2) {
	    REQUIRE(RMB_IN_1[0].size() == 0);
	    REQUIRE(RMB_IN_1[1].size() == 2);
	    if(RMB_IN_1[1].size() == 2) {
	      REQUIRE(RMB_IN_1[1][0].offset() == 32832);
	      REQUIRE(RMB_IN_1[1][1].offset() == 32960);
	    }
	   
	  }
	  REQUIRE(RMB_OUT_1.size() == 2);
	  if(RMB_OUT_1.size() == 2) {
	    REQUIRE(RMB_OUT_1[1].size() == 2);
	    if(RMB_OUT_1[1].size() == 2) {
	      REQUIRE(RMB_OUT_1[1][0].offset() == 32832);
	      REQUIRE(RMB_OUT_1[1][1].offset() == 32960);
	    }
	    REQUIRE(RMB_OUT_1[0].size() == 1);
	    if(RMB_OUT_1[0].size() == 1) {
	      REQUIRE(RMB_OUT_1[0][0].offset() == 32960);
	    }
	  }
	  
	}
	// Block 3
	if(mb->block_id == 3) {
	  std::vector<std::vector<Address>> RMB_IN_0 = mb->rmb_in[0];
	  std::vector<std::vector<Address>> RMB_IN_1 = mb->rmb_in[1];
	  std::vector<std::vector<Address>> RMB_OUT_0 = mb->rmb_out[0];
	  std::vector<std::vector<Address>> RMB_OUT_1 = mb->rmb_out[1];
	  
	  // Index 0
	  REQUIRE(RMB_IN_0.size() == 2);
	  if(RMB_IN_0.size() == 2) {
	    REQUIRE(RMB_IN_0[0].size() == 1);
	    if(RMB_IN_0[0].size() == 1) {
	      REQUIRE(RMB_IN_0[0][0].offset() == 32768);
	    }
	    REQUIRE(RMB_IN_0[1].size() == 2);
	    if(RMB_IN_0[1].size() == 2) {
	      REQUIRE(RMB_IN_0[1][0].offset() == 32768);
	      REQUIRE(RMB_IN_0[1][1].offset() == 32896);
	    }
	  }
	  REQUIRE(RMB_OUT_0.size() == 1);
	  if(RMB_OUT_0.size() == 1) {
	    REQUIRE(RMB_OUT_0[0].size() == 2);
	    if(RMB_OUT_0[0].size() == 2) {
	      REQUIRE(RMB_OUT_0[0][0].offset() == 32768);
	      REQUIRE(RMB_OUT_0[0][1].offset() == 32896);
	    }
	  }
	  // Index 1
	  REQUIRE(RMB_IN_1.size() == 2);
	  if(RMB_IN_1.size() == 2) {
	    REQUIRE(RMB_IN_1[1].size() == 2);
	    if(RMB_IN_1[1].size() == 2) {
	      REQUIRE(RMB_IN_1[1][0].offset() == 32832);
	      REQUIRE(RMB_IN_1[1][1].offset() == 32960);
	    }
	    REQUIRE(RMB_IN_1[0].size() == 1);
	    if(RMB_IN_1[0].size() == 2) {
	      REQUIRE(RMB_IN_1[0][0].offset() == 32960);
	    }
	  }
	  REQUIRE(RMB_OUT_1.size() == 1);
	  if(RMB_OUT_1.size() == 1) {
	    REQUIRE(RMB_OUT_1[0].size() == 2);
	    if(RMB_OUT_1[0].size() == 2) {
	      REQUIRE(RMB_OUT_1[0][0].offset() == 32832);
	      REQUIRE(RMB_OUT_1[0][1].offset() == 32960);
	    }
	  }
	}
	// Block 4
	if(mb->block_id == 4) {
	  std::vector<std::vector<Address>> RMB_IN_0 = mb->rmb_in[0];
	  std::vector<std::vector<Address>> RMB_IN_1 = mb->rmb_in[1];
	  std::vector<std::vector<Address>> RMB_OUT_0 = mb->rmb_out[0];
	  std::vector<std::vector<Address>> RMB_OUT_1 = mb->rmb_out[1];
	  
	  // Index 0
	  REQUIRE(RMB_IN_0.size() == 2);
	  if(RMB_IN_0.size() == 2) {
	    REQUIRE(RMB_IN_0[0].size() == 1);
	    if(RMB_IN_0[0].size() == 1) {
	      REQUIRE(RMB_IN_0[0][0].offset() == 32768);
	    }
	    REQUIRE(RMB_IN_0[1].size() == 2);
	    if(RMB_IN_0[1].size() == 2) {
	      REQUIRE(RMB_IN_0[1][0].offset() == 32768);
	      REQUIRE(RMB_IN_0[1][1].offset() == 32896);
	    }
	  }
	  REQUIRE(RMB_OUT_0.size() == 2);
	  if(RMB_OUT_0.size() == 2) {
	    REQUIRE(RMB_OUT_0[0].size() == 1);
	    if(RMB_OUT_0[0].size() == 1) {
	      REQUIRE(RMB_OUT_0[0][0].offset() == 32768);
	    }
	    REQUIRE(RMB_OUT_0[1].size() == 2);
	    if(RMB_OUT_0[1].size() == 2) {
	      REQUIRE(RMB_OUT_0[1][0].offset() == 32768);
	      REQUIRE(RMB_OUT_0[1][1].offset() == 32896);
	    }
	  }
	  // Index 1
	  REQUIRE(RMB_IN_1.size() == 2);
	  if(RMB_IN_1.size() == 2) {
	    REQUIRE(RMB_IN_1[1].size() == 2);
	    if(RMB_IN_1[1].size() == 2) {
	      REQUIRE(RMB_IN_1[1][0].offset() == 32832);
	      REQUIRE(RMB_IN_1[1][1].offset() == 32960);
	    }
	    REQUIRE(RMB_IN_1[0].size() == 1);
	    if(RMB_IN_1[0].size() == 1) {
	      REQUIRE(RMB_IN_1[0][0].offset() == 32960);
	    }
	  }
	  REQUIRE(RMB_OUT_1.size() == 2);
	  if(RMB_OUT_1.size() == 2) {
	    REQUIRE(RMB_OUT_1[1].size() == 2);
	    if(RMB_OUT_1[1].size() == 2) {
	      REQUIRE(RMB_OUT_1[1][0].offset() == 32832);
	      REQUIRE(RMB_OUT_1[1][1].offset() == 32960);
	    }
	    REQUIRE(RMB_OUT_1[0].size() == 1);
	    if(RMB_OUT_1[0].size() == 1) {
	      REQUIRE(RMB_OUT_1[0][0].offset() == 32960);
	    }
	  }
	}
      }
    }
  }

}

TEST_CASE("LMB","[Memory Block]") {

  WorkSpace *ws = NULL;
  PropList conf;
  Manager manager;
  NO_SYSTEM(conf) = true;
  TASK_ENTRY(conf) = "main";
  VERBOSE(conf) = true;
 
  CACHE_CONFIG_PATH(conf) = "./cache_large_code.xml";
  StringBuffer buf;
  buf << "test_condition-decomp.c";
  elm::io::OutFileStream s(buf.toString());
  elm::io::Output out(s);
  conf.print(out);
  ws = manager.load("large_loop", conf);

  ws->require(DynFeature("otawa::initialize_RMB_LMB::INITIALIZE_RMB_LMB_FEATURE"), conf);

 

  
  const CFGCollection *coll = INVOLVED_CFGS(ws);
  for(CFGCollection::Iter cfg_iter(*coll); cfg_iter(); cfg_iter++) {
    for(CFG::BlockIter block_iter = cfg_iter->blocks(); block_iter(); block_iter++) {
      if(block_iter->isBasic()) {
	BasicBlock *bb = block_iter->toBasic();
        
	struct Memory_block *mb = MB_STRUCT(bb);
	//Block 1
	if(mb->block_id == 1) {
	  std::cout << "BB1" << std::endl;
	  std::vector<std::vector<Address>> LMB_IN_0 = mb->lmb_in[0];
	  std::vector<std::vector<Address>> LMB_IN_1 = mb->lmb_in[1];
	  std::vector<std::vector<Address>> LMB_OUT_0 = mb->lmb_out[0];
	  std::vector<std::vector<Address>> LMB_OUT_1 = mb->lmb_out[1];
	  // Index 0
	  REQUIRE(LMB_IN_0.size() == 2);
	  if(LMB_IN_0.size() == 2) {
	    REQUIRE(LMB_IN_0[0].size() == 2);
	    if(LMB_IN_0[0].size() == 2) {
	      REQUIRE(LMB_IN_0[0][0].offset() == 32768);
	      REQUIRE(LMB_IN_0[0][1].offset() == 32896);
	    }
	    REQUIRE(LMB_IN_0[1].size() == 1);
	    if(LMB_IN_0[1].size() == 1) {
	      REQUIRE(LMB_IN_0[1][0].offset() == 32768);
	    }
	  }
	  REQUIRE(LMB_OUT_0.size() == 2);
	  if(LMB_OUT_0.size() == 2) {
	    REQUIRE(LMB_OUT_0[0].size() == 2);
	    if(LMB_OUT_0[0].size() == 2) {
	      REQUIRE(LMB_OUT_0[0][0].offset() == 32768);
	      REQUIRE(LMB_OUT_0[0][1].offset() == 32896);
	    }
	    REQUIRE(LMB_OUT_0[1].size() == 0);
	  }
	  // Index 1
	  REQUIRE(LMB_IN_1.size() == 2);
	  if(LMB_IN_1.size() == 2) {
	    REQUIRE(LMB_IN_1[0].size() == 2);
	    if(LMB_IN_1[0].size() == 2) {
	      REQUIRE(LMB_IN_1[0][0].offset() == 32960);
	      REQUIRE(LMB_IN_1[0][1].offset() == 32832);
	    }
	    REQUIRE(LMB_IN_1[1].size() == 1);
	    if(LMB_IN_1[1].size() == 1) {
	      REQUIRE(LMB_IN_1[1][0].offset() == 32960);
	    }
	  }
	  REQUIRE(LMB_OUT_1.size() == 2);
	  if(LMB_OUT_1.size() == 2) {
	    REQUIRE(LMB_OUT_1[0].size() == 2);
	    if(LMB_OUT_1[0].size() == 2) {
	      REQUIRE(LMB_OUT_1[0][0].offset() == 32960);
	      REQUIRE(LMB_OUT_1[0][1].offset() == 32832);
	    }
	    REQUIRE(LMB_OUT_1[1].size() == 1);
	    if(LMB_OUT_1[1].size() == 1) {
	      REQUIRE(LMB_OUT_1[1][0].offset() == 32960);
	    }
	  }
	 
	}

	// Block 2
	if(mb->block_id == 2) {
	  std::cout << "BB2" << std::endl;
	  std::vector<std::vector<Address>> LMB_IN_0 = mb->lmb_in[0];
	  std::vector<std::vector<Address>> LMB_IN_1 = mb->lmb_in[1];
	  std::vector<std::vector<Address>> LMB_OUT_0 = mb->lmb_out[0];
	  std::vector<std::vector<Address>> LMB_OUT_1 = mb->lmb_out[1];
	  // Index 0
	  REQUIRE(LMB_IN_0.size() == 2);
	  if(LMB_IN_0.size() == 2) {
	    
	    REQUIRE(LMB_IN_0[0].size() == 2);
	    if(LMB_IN_0[0].size() == 2) {
	      REQUIRE(LMB_IN_0[0][0].offset() == 32768);
	      REQUIRE(LMB_IN_0[0][1].offset() == 32896);
	    }
	    REQUIRE(LMB_IN_0[1].size() == 0);
	  }
	  REQUIRE(LMB_OUT_0.size() == 2);
	  if(LMB_OUT_0.size() == 2) {
	    
	    REQUIRE(LMB_OUT_0[0].size() == 2);
	    if(LMB_OUT_0[0].size() == 2) {
	      REQUIRE(LMB_OUT_0[0][0].offset() == 32768);
	      REQUIRE(LMB_OUT_0[0][1].offset() == 32896);
	    }
	    REQUIRE(LMB_OUT_0[1].size() == 0);
	  }
	  
	  // Index 1
	  REQUIRE(LMB_IN_1.size() == 2);
	  if(LMB_IN_1.size() == 2) {
	    REQUIRE(LMB_IN_1[0].size() == 2);
	    if(LMB_IN_1[0].size() == 2) {
	      REQUIRE(LMB_IN_1[0][0].offset() == 32960);
	      REQUIRE(LMB_IN_1[0][1].offset() == 32832);
	    }
	    REQUIRE(LMB_IN_1[1].size() == 1);
	    if(LMB_IN_1[1].size() == 1) {
	      REQUIRE(LMB_IN_1[1][0].offset() == 32960);
	    }
	  }
	 
	  REQUIRE(LMB_OUT_1.size() == 2);
	  if(LMB_OUT_1.size() == 2) {
	    REQUIRE(LMB_OUT_1[0].size() == 2);
	    if(LMB_OUT_1[0].size() == 2) {
	      REQUIRE(LMB_OUT_1[0][0].offset() == 32832);
	      REQUIRE(LMB_OUT_1[0][1].offset() == 32960);
	    }
	    REQUIRE(LMB_OUT_1[1].size() == 1);
	    if(LMB_OUT_1[1].size() == 1) {
	      REQUIRE(LMB_OUT_1[1][0].offset() == 32960);
	    }
	  }
	  
	}
	// Block 3
	if(mb->block_id == 3) {
	  std::cout << "BB3" << std::endl;
	  std::vector<std::vector<Address>> LMB_IN_0 = mb->lmb_in[0];
	  std::vector<std::vector<Address>> LMB_IN_1 = mb->lmb_in[1];
	  std::vector<std::vector<Address>> LMB_OUT_0 = mb->lmb_out[0];
	  std::vector<std::vector<Address>> LMB_OUT_1 = mb->lmb_out[1];
	  // Index 0
	  REQUIRE(LMB_IN_0.size() == 1);
	  if(LMB_IN_0.size() == 1) {
	    REQUIRE(LMB_IN_0[0].size() == 2);
	    if(LMB_IN_0[0].size() == 2) {
	      REQUIRE(LMB_IN_0[0][0].offset() == 32768);
	      REQUIRE(LMB_IN_0[0][1].offset() == 32896);
	    }
	  }
	  REQUIRE(LMB_OUT_0.size() == 2);
	  if(LMB_OUT_0.size() == 2) {
	    
	    REQUIRE(LMB_OUT_0[0].size() == 2);
	    if(LMB_OUT_0[0].size() == 2) {
	      REQUIRE(LMB_OUT_0[0][0].offset() == 32768);
	      REQUIRE(LMB_OUT_0[0][1].offset() == 32896);
	    }
	    REQUIRE(LMB_OUT_0[1].size() == 0);
	  }
	  // Index 1
	  REQUIRE(LMB_IN_1.size() == 1);
	  if(LMB_IN_1.size() == 1) {
	    REQUIRE(LMB_IN_1[0].size() == 2);
	    if(LMB_IN_1[0].size() == 2) {
	      REQUIRE(LMB_IN_1[0][0].offset() == 32832);
	      REQUIRE(LMB_IN_1[0][1].offset() == 32960);
	    }
	  }
	  
	  REQUIRE(LMB_OUT_1.size() == 2);
	  if(LMB_OUT_1.size() == 2) {
	    REQUIRE(LMB_OUT_1[0].size() == 2);
	    if(LMB_OUT_1[0].size() == 2) {
	      REQUIRE(LMB_OUT_1[0][0].offset() == 32960);
	      REQUIRE(LMB_OUT_1[0][1].offset() == 32832);
	    }
	    REQUIRE(LMB_OUT_1[1].size() == 1);
	    if(LMB_OUT_1[1].size() == 1) {
	      REQUIRE(LMB_OUT_1[1][0].offset() == 32960);
	    }
	  }
	  
	}
	// Block 4
	if(mb->block_id == 4) {
	  std::cout << "BB4" << std::endl;
	  std::vector<std::vector<Address>> LMB_IN_0 = mb->lmb_in[0];
	  std::vector<std::vector<Address>> LMB_IN_1 = mb->lmb_in[1];
	  std::vector<std::vector<Address>> LMB_OUT_0 = mb->lmb_out[0];
	  std::vector<std::vector<Address>> LMB_OUT_1 = mb->lmb_out[1];
	  
	  // Index 0
	  REQUIRE(LMB_IN_0.size() == 1);
	  if(LMB_IN_0.size() == 1) {
	    REQUIRE(LMB_IN_0[0].size() == 0);
	  }
	  REQUIRE(LMB_OUT_0.size() == 0);
	  // Index 1
	  REQUIRE(LMB_IN_1.size() == 1);
	  if(LMB_IN_1.size() == 1) {
	    REQUIRE(LMB_IN_1[0].size() == 1);
	    if(LMB_IN_1[0].size() == 1) {
	      REQUIRE(LMB_IN_1[0][0].offset() == 32960);
	    }
	  }
	  REQUIRE(LMB_OUT_1.size() == 0);
	  std::cout << "BB4_bis" << std::endl;
	  //LMB_OUT_0
	  REQUIRE(LMB_OUT_0.size() == 0);
	  std::cout << "BB4_bis2" << std::endl;
	  //LMB_OUT_1
	  REQUIRE(LMB_OUT_1.size() == 0);
	   std::cout << "BB4_bis3" << std::endl;
	}
      }
    }
  }

}

