
#include "include/initialize_RMB_LMB.h"
//#include "include/cache_block_id.h"
#include "include/type.h"
#include <otawa/proc/DynFeature.h>
#include <otawa/hard/CacheConfiguration.h>
#include <otawa/cache/features.h>

#include <otawa/cfg/CFG.h>
#include <otawa/cache/features.h>
#include <otawa/cache/cat2/CAT2Builder.h>
#include <otawa/cache/LBlockBuilder.h>

#include <otawa/ipet/features.h>
#include <otawa/ipet/FlowFactLoader.h>

#include <otawa/flowfact/features.h>

//#include <multiset.h>



namespace otawa
{
  namespace initialize_RMB_LMB
  {

    // Declaration du plugin

    class Plugin : public ProcessorPlugin
    {
    public:
      Plugin() : ProcessorPlugin(
				 "otawa::initialize_RMB_LMB",
				 Version(1, 0, 0),
				 OTAWA_PROC_VERSION) {}
    };

    otawa::initialize_RMB_LMB::Plugin initialize_RMB_LMB_plugin;
    ELM_PLUGIN(initialize_RMB_LMB_plugin, OTAWA_PROC_HOOK);

    p::declare Initialize_RMB_LMBProcessor::reg = p::init(
							  "otawa::initialize_RMB_LMB::Initialize_RMB_LMBProcessor",
							  Version(1, 0, 0))
      .require(COLLECTED_CFG_FEATURE)
      .require(LOOP_INFO_FEATURE)
      .require(ICACHE_CATEGORY2_FEATURE)
      .require(ipet::FLOW_FACTS_FEATURE)
      .require(MKFF_PRESERVATION_FEATURE)
      .require(cache::COLLECTED_LBLOCKS_FEATURE)
      .require(hard::CACHE_CONFIGURATION_FEATURE)
      .provide(INITIALIZE_RMB_LMB_FEATURE);
    //.require(otawa::DynFeature("otawa::cache_block_id::CACHE_BLOCK_ID_FEATURE"))
    // definition feature
    p::feature INITIALIZE_RMB_LMB_FEATURE(
					  "otawa::initialize_RMB_LMB::INITIALIZE_RMB_LMB_FEATURE",
					  new Maker<Initialize_RMB_LMBProcessor>());

    Initialize_RMB_LMBProcessor::Initialize_RMB_LMBProcessor(
							     p::declare &r) : Processor(r) {}

    void Initialize_RMB_LMBProcessor::configure(
						const PropList &props)
    {
      Processor::configure(props);
    }


   

    void print_memory_block(struct Memory_block *mb, int nb_index) {
      //std::cout << "-----------------------" << std::endl;
      //std::cout << "Block:" << mb->block_id << std::endl;
      //std::cout << "RMB_IN" << std::endl;
      for(int i = 0; i < nb_index; i++) {
	//std::cout <<"[" << i << "]" << " ";
	std::vector<std::vector<Address>> tmp_set = get_set(mb->rmb_in[i]);
	for(int j = 0; j < tmp_set.size(); j++) {
	  //std::cout << "{";
	  for(int k = 0; k < tmp_set[j].size(); k++) {
	    //std::cout << tmp_set[j][k].offset() << " ";
	  }
	  //std::cout << "}";
	}
	//std::cout << std::endl;
      }
      //std::cout << std::endl;
      //std::cout << "RMB_OUT" << std::endl;
      for(int i = 0; i < nb_index; i++) {
	//std::cout <<"[" << i << "]" << " ";
	std::vector<std::vector<Address>> tmp_set = get_set(mb->rmb_out[i]);
	for(int j = 0; j < tmp_set.size(); j++) {
	  //std::cout << "{";
	  for(int k = 0; k < tmp_set[j].size(); k++) {
	    //std::cout << tmp_set[j][k].offset() << " ";
	  }
	  //std::cout << "}";
	}
	//std::cout << std::endl;
      }
      //std::cout << std::endl;
      //std::cout << "-----------------------" << std::endl;
    }

    

    bool not_present_in_vect(std::vector<Block *> visited, Block *b)
    {
      for (int i = 0; i < visited.size(); i++)
	{
	  if (visited[i] == b)
	    {
	      return false;
	    }
	}
      return true;
    }

    int get_exit_id_of_cfg(CFG *cfg)
    {
      for (CFG::BlockIter block_iter = cfg->blocks(); block_iter(); block_iter++)
	{
	  Block *b = *block_iter;
	  bool found = true;
	  for (
	       Block::EdgeIter iter_suc(b->outs());
	       iter_suc();
	       iter_suc++)
	    {
	      found = false;
	      break;
	    }
	  if (found)
	    {
	      return b->id();
	    }
	}
      return -1;
    }

    std::vector<Block *> get_last_blocks(CFG *cfg)
    {
      std::vector<Block *> tmp_v;
      int id_last = get_exit_id_of_cfg(cfg);
      if (id_last == -1)
	{
	  return tmp_v;
	}
      for (CFG::BlockIter block_iter = cfg->blocks(); block_iter(); block_iter++)
	{
	  Block *b = *block_iter;

	  for (
	       Block::EdgeIter iter_suc(b->outs());
	       iter_suc();
	       iter_suc++)
	    {
	      Edge *edge = *iter_suc;
	      Block *target = edge->target();
	      if (target->id() == id_last)
		{
		  tmp_v.push_back(b);
		  break;
		}
	    }
	}
      return tmp_v;
    }

    
    void mark_first_bb(Block *b_fct_entry, struct Memory_block *mb, std::vector<Block *> visited)
    {
      visited.push_back(b_fct_entry);
      if (b_fct_entry->isBasic())
	{
	  struct Memory_block *mb_suc = MB_STRUCT(b_fct_entry);
	  mb->suc.push_back(mb_suc);
	  mb_suc->pred.push_back(mb);
	}
      else
	{

	  if (b_fct_entry->isSynth())
	    {
	      std::vector<Block *> v_tmp;
	      mark_first_bb(b_fct_entry->toSynth()->callee()->entry(), mb, v_tmp);
	    }
	  else
	    {

	      for (Block::EdgeIter iter_suc(b_fct_entry->outs());
		   iter_suc();
		   iter_suc++)
		{

		  Edge *edge = *iter_suc;
		  Block *target = edge->target();
		  if (not_present_in_vect(visited, target))
		    {
		      mark_first_bb(target, mb, visited);
		    }
		}
	    }
	}
    }

    void mark_last_bb_bis(Block *b_fct_exit, struct Memory_block *mb, std::vector<Block *> visited)
    {
      visited.push_back(b_fct_exit);
      //std::cout << "Block: " << b_fct_exit->id() << std::endl;
      if (b_fct_exit->isBasic())
	{
	  struct Memory_block *mb_pred = MB_STRUCT(b_fct_exit);
	  //std::cout << "----------------------------- FOUND pred" << std::endl;
	  mb_pred->suc.push_back(mb);
	  mb->pred.push_back(mb_pred);
	}
      else
	{
	  if (b_fct_exit->isSynth())
	    {
	      //std::cout << "is synth" << std::endl;
	      std::vector<Block *> v_tmp;
	      mark_last_bb_bis(b_fct_exit->toSynth()->callee()->exit(), mb, v_tmp);
	    }
	  else
	    {

	      for (Block::EdgeIter iter_pred(b_fct_exit->ins());
		   iter_pred();
		   iter_pred++)
		{
		  //std::cout << "here" << std::endl;
		  Edge *edge = *iter_pred;
		  Block *target = edge->source();
		  //std::cout << "Block: " << target->id() << std::endl;
		  if (not_present_in_vect(visited, target))
		    {
		      //std::cout << "not present" << std::endl;
		      mark_last_bb_bis(target, mb, visited);
		    }
		  else
		    {
		      //std::cout << "present in list" << std::endl;
		    }
		}
	    }
	}
    }

    void mark_last_bb(Block *b_fct_exit, Block *main_suc, std::vector<Block *> visited)
    {
      visited.push_back(main_suc);
      if (main_suc->isBasic())
	{
	  struct Memory_block *mb = MB_STRUCT(main_suc);
	  std::vector<Block *> v_tmp;
	  //std::cout << "----------------------------- FOUND suc" << std::endl;
	  mark_last_bb_bis(b_fct_exit, mb, v_tmp);
	}
      else
	{
	  if (main_suc->isSynth())
	    {
	      std::vector<Block *> v_tmp;
	      mark_last_bb(b_fct_exit, main_suc->toSynth()->callee()->entry(), v_tmp);
	    }
	  else
	    {

	      for (Block::EdgeIter iter_suc(main_suc->outs());
		   iter_suc();
		   iter_suc++)
		{
		  Edge *edge = *iter_suc;
		  Block *target = edge->target();
		  if (not_present_in_vect(visited, target))
		    {

		      mark_last_bb(b_fct_exit, target, visited);
		    }
		}
	    }
	}
    }

    void add_links(Block *b, struct Memory_block *mb, std::vector<Block *> visited)
    {
      visited.push_back(b);

      if (b->isBasic())
	{
	  BasicBlock *bb = b->toBasic();
	  struct Memory_block *mb_suc = MB_STRUCT(b);
	  mb->suc.push_back(mb_suc);
	  mb_suc->pred.push_back(mb);
	}
      else
	{
	  if (b->isSynth())
	    {
	      SynthBlock *sb = b->toSynth();
	      CFG *cfg_fct = sb->callee();

	      Block *b_fct_entry = cfg_fct->entry();
	      //Block *b_fct_exit = cfg_fct->exit();
	      std::vector<Block *> exits = get_last_blocks(cfg_fct);
	      std::vector<Block *> v_tmp;
	      mark_first_bb(b_fct_entry, mb, v_tmp);

	      for (Block::EdgeIter iter_suc(b->outs());
		   iter_suc();
		   iter_suc++)
		{
		  Edge *edge = *iter_suc;
		  Block *target = edge->target();
		  std::vector<Block *> v_tmp_bis;
		  for (int i = 0; i < exits.size(); i++)
		    {
		      mark_last_bb(exits[i], target, v_tmp_bis);
		    }
		}
	    }
	  else
	    {

	      for (Block::EdgeIter iter_suc(b->outs());
		   iter_suc();
		   iter_suc++)
		{
		  Edge *edge = *iter_suc;
		  Block *target = edge->target();
		  if (not_present_in_vect(visited, target))
		    {
		      add_links(target, mb, visited);
		    }
		}
	    }
	}
    }

    //----------------------------------------------------------------

    // TRUE LRU

    std::vector<Address> add_LRU(std::vector<Address> cache, Address elem, int nb_way) {

      // if elem already in cache
      bool is_present = false;
      for(int i = 0; i < cache.size(); i++) {
	if(cache[i] == elem) {
	  cache.erase(cache.begin() + i);
	  break;
	}
      }

      cache.push_back(elem);
      while(cache.size() > nb_way) {
	cache.erase(cache.begin());
      }

      return cache;

      
    }

    std::vector<Address> add_LRU_bis(std::vector<Address> cache, Address elem, int nb_way) {

      // if elem already in cache
      bool is_present = false;
      for(int i = 0; i < cache.size(); i++) {
	if(cache[i] == elem) {
	  cache.erase(cache.begin() + i);
	  break;
	}
      }

      cache.insert(cache.begin(), elem);
      while(cache.size() > nb_way) {
	cache.pop_back();
      }

      return cache;

      
    }
    
    std::vector<Address> rmb_LRU(std::vector<Address> gen, std::vector<Address> rmb, int nb_way)
    {
      for(int i = 0; i < gen.size(); i++) {
	rmb = add_LRU(rmb, gen[i], nb_way);
      }
      return rmb;
    }

    std::vector<Address> lmb_LRU(std::vector<Address> gen, std::vector<Address> lmb, int nb_way)
    {

      for(int i = (gen.size()-1); i >= 0; i--) {
	lmb = add_LRU_bis(lmb, gen[i], nb_way);
      }
      return lmb;
    }
    
    //----------------------------------------------------------------
    
    // std::vector<Address> rmb_LRU(std::vector<Address> gen, std::vector<Address> rmb, int nb_way)
    // {
      
    //   int i = rmb.size() - 1;
    //   while (gen.size() < nb_way && i >= 0)
    // 	{
    // 	  gen.insert(gen.begin(), rmb[i]);
    // 	  i--;
    // 	}
    //   return gen;
    // }

    // std::vector<Address> lmb_LRU(std::vector<Address> gen, std::vector<Address> rmb, int nb_way)
    // {

    //   int i = 0;

    //   while (gen.size() < nb_way && i < rmb.size())
    // 	{
    // 	  gen.push_back(rmb[i]);
    // 	  i++;
    // 	}
    //   return gen;
    // }
    void print_true_gen(struct Memory_block *mb, int nb_index) {
      // std::cout << "-----------------------" << std::endl;
      // std::cout << "id=" << mb->block_id << std::endl;
      // //std::cout << "suc: ";
      // for(int i = 0; i < mb->suc.size(); i++) {
      // 	//std::cout << mb->suc[i]->block_id << " ";
      // }
      // //std::cout << std::endl << "--lmb_gen--";
      
      // // for (int i = 0; i < nb_index;i++)
      // // 	{
      // // 	  std::cout << std::endl << "[" << i << "]=";
      // // 	  for(int j = 0; j < mb->lmb_gen[i].size(); j++) {
      // // 	    std::cout << " " << mb->lmb_gen[i][j].page() << ":" << mb->lmb_gen[i][j].offset();
      // // 	  }
      // // 	}
      // std::cout << std::endl << "--rmb_gen--";
      
      // for (int i = 0; i < nb_index;i++) {
      // 	std::cout << std::endl << "[" << i << "]=";
      // 	for(int j = 0; j < mb->rmb_gen[i].size(); j++) {
      // 	  std::cout << " " << mb->rmb_gen[i][j].page() << ":" << mb->rmb_gen[i][j].offset();
      // 	}
      // }
      // std::cout << std::endl << "-----------------------" << std::endl;
    }
    

    void init_gen(BasicBlock *bb, int nb_index, int nb_way, const otawa::hard::Cache *cache) {
      Memory_block *mb = MB_STRUCT(bb);
      std::vector<Address> vect[nb_index];
      AllocArray<LBlock *> *lblock_array = BB_LBLOCKS(bb);

      for (int i = 0; i < lblock_array->count(); i++) {
	LBlock *lb = (*lblock_array)[i];
	otawa::Address lb_id = cache->round(lb->address());
	vect[cache->line(lb->address())].push_back(lb_id);
      }

      // for(int i = 0; i < nb_index; i++) {
      // 	for(int j = 0; j < vect[i].size(); j++) {
      // 	  std::cout << vect[i][j].offset() << " ";
      // 	}
      // 	std::cout << std::endl;
      // }
      
      for (int index_tmp = 0; index_tmp < nb_index; index_tmp++) {
	for (int i = 0; i < nb_way; i++) {
	  if (i < vect[index_tmp].size()) {
	    mb->lmb_gen[index_tmp].push_back(vect[index_tmp][i]);
	  }
	  int j = vect[index_tmp].size() - 1 - i;
	  if (j >= 0) {
	    mb->rmb_gen[index_tmp].insert(mb->rmb_gen[index_tmp].begin(), vect[index_tmp][j]);
	  }
	}

	if(mb->lmb_gen[index_tmp].size() != 0)
	  mb->lmb_in[index_tmp].push_back(mb->lmb_gen[index_tmp]);
	
	if(mb->rmb_gen[index_tmp].size() != 0)
	  mb->rmb_out[index_tmp].push_back(mb->rmb_gen[index_tmp]);
      }
      print_true_gen(mb, nb_index);
    }
    
   
    void print_gen(struct Memory_block *mb,int nb_index ,int nb_way)
    {
      // std::cout << "-----------------------" << std::endl;
      // std::cout << "id=" << mb->block_id << std::endl;
      // std::cout << "--RMB--" << std::endl;
      // for (int i = 0; i < nb_index; i++) {
      // 	std::cout << "size rmb_gen[" << i << "]=" << mb->rmb_gen[i].size() << std::endl;
      // }

      // for (int i = 0; i < nb_index; i++) {
      // 	std::cout << "size rmb_in[" << i << "]=" << mb->rmb_in[i].size() << std::endl;
      // 	std::vector<std::vector<Address>> tmp = get_set(mb->rmb_in[i]);
      // 	for (int j = 0; j < tmp.size(); j++) {
      // 	  std::cout << "size rmb_in[" << i << "][" << j << "]=" << tmp[j].size() << std::endl;
      // 	}
      // }
      // for (int i = 0; i < nb_index; i++) {
      // 	std::cout << "size rmb_out[" << i << "]=" << mb->rmb_out[i].size() << std::endl;
      // 	std::vector<std::vector<Address>> tmp = get_set(mb->rmb_out[i]);
      // 	for (int j = 0; j < tmp.size(); j++) {
      // 	  std::cout << "size rmb_out[" << i << "][" << j << "]=" << tmp[j].size() << std::endl;
      // 	}
      // }

      // std::cout << "--LMB--" << std::endl;
      // for (int i = 0; i < nb_index; i++) {
      // 	std::cout << "size lmb_gen[" << i << "]=" << mb->lmb_gen[i].size() << std::endl;
      // }

      // for (int i = 0; i < nb_index; i++) {
      // 	std::cout << "size lmb_in[" << i << "]=" << mb->lmb_in[i].size() << std::endl;
      // 	std::vector<std::vector<Address>> tmp = get_set(mb->lmb_in[i]);
      // 	for (int j = 0; j < tmp.size(); j++) {
      // 	  std::cout << "size lmb_in[" << i << "][" << j << "]=" << tmp[j].size() << std::endl;
      // 	  for(int k = 0; k < tmp[j].size(); k++) {
      // 	    std::cout << "--" << tmp[j][k].offset() << std::endl;
      // 	  }
      // 	}
      // }
      // for (int i = 0; i < nb_index; i++) {
      // 	std::cout << "size lmb_out[" << i << "]=" << mb->lmb_out[i].size() << std::endl;
      // 	std::vector<std::vector<Address>> tmp = get_set(mb->lmb_out[i]);
      // 	for (int j = 0; j < tmp.size(); j++) {
      // 	  std::cout << "size lmb_out[" << i << "][" << j << "]=" << tmp[j].size() << std::endl;
      // 	  for(int k = 0; k < tmp[j].size(); k++) {
      // 	    std::cout << "--" << tmp[j][k].offset() << std::endl;
      // 	  }
      // 	}
      // }
      
      // std::cout << "-----------------------" << std::endl;
    }

    //////////////////////////////////////////////////////////
    //                       Processor                      //
    //////////////////////////////////////////////////////////


    void create_structure(const CFGCollection *coll, int nb_index, int nb_way) {
      for (CFGCollection::Iter cfg_iter(*coll); cfg_iter(); cfg_iter++)
	{
	  for (CFG::BlockIter block_iter = cfg_iter->blocks(); block_iter(); block_iter++)
	    {
	      if (block_iter->isBasic())
		{
		  BasicBlock *bb = block_iter->toBasic();
		  MB_STRUCT(bb) = new Memory_block(nb_index, nb_way, bb);
		}
	    }
	}

    }

    void link_structure(const CFGCollection *coll, int nb_way, int nb_index) {
      for (CFGCollection::Iter cfg_iter(*coll); cfg_iter(); cfg_iter++)
	{
	  for (CFG::BlockIter block_iter = cfg_iter->blocks(); block_iter(); block_iter++)
	    {
	      if (block_iter->isBasic())
		{
		  BasicBlock *bb = block_iter->toBasic();

		  Memory_block *mb = MB_STRUCT(bb);
		  mb->block_id = bb->id();
		  for (
		       Block::EdgeIter iter_suc(bb->outs());
		       iter_suc();
		       iter_suc++)
		    {

		      Edge *edge = *iter_suc;
		      Block *target = edge->target();
		      std::vector<Block *> visited;
		      add_links(target, mb, visited);
		    }
		}
	    }
	}

    }

    void initialize_structure(const CFGCollection *coll, int nb_way, int nb_index, const hard::Cache *cache) {
      for (CFGCollection::Iter cfg_iter(*coll); cfg_iter(); cfg_iter++)
	{
	  for (CFG::BlockIter block_iter = cfg_iter->blocks(); block_iter(); block_iter++)
	    {
	      if (block_iter->isBasic())
		{
		  BasicBlock *bb = block_iter->toBasic();
		  Memory_block *mb = MB_STRUCT(bb);
				

		  init_gen(bb, nb_index, nb_way, cache);
		}
	    }
	}
    }

    void compute_rmb(const CFGCollection *coll, int nb_way, int nb_index) {
      bool change = true;
      while(change) {

	change = false;
	  
	for(CFGCollection::Iter cfg_iter(*coll); cfg_iter(); cfg_iter++) {
	  for(CFG::BlockIter block_iter = cfg_iter->blocks(); block_iter(); block_iter++) {

	    // ----- for each BB -----
	    if(block_iter->isBasic()) {
	      BasicBlock *bb = block_iter->toBasic();
	      Memory_block *mb = MB_STRUCT(bb);

	      // ----- for each row -----
	      for (int index = 0; index < nb_index; index++) {

		// RMB IN
		mb->rmb_in[index].clear();
		for (int pred_mb = 0; pred_mb < mb->pred.size(); pred_mb++) {
		  mb->rmb_in[index] =
		    get_set(vector_union(mb->rmb_in[index],
					 mb->pred[pred_mb]->rmb_out[index]));	  
		}

		// RMB OUT
		std::vector<std::vector<Address>> oldout(mb->rmb_out[index]);
		mb->rmb_out[index].clear();
	
		std::vector<std::vector<Address>> rmb_in = mb->rmb_in[index];
		if(rmb_in.size() == 0) {
		  // TODO: Need to modify this part to take into account empty gen
		  //if(mb->rmb_gen[index].size() != 0) {
		    mb->rmb_out[index].push_back(mb->rmb_gen[index]);
		    //}
		} else {
		  for (int tmp_rmb = 0; tmp_rmb < rmb_in.size(); tmp_rmb++) {
		    std::vector<Address> tmp_vect = mb->rmb_gen[index];
		    rmb_in[tmp_rmb] = get_set(rmb_LRU(tmp_vect, rmb_in[tmp_rmb], nb_way));
		    mb->rmb_out[index].push_back(rmb_in[tmp_rmb]);
		  }
		}

		mb->rmb_out[index] = get_set(mb->rmb_out[index]);
		
		// UPDATE END CONDITION
		if (oldout != mb->rmb_out[index]) {
		  change = true;
		}
		
	      }
	    }
	  }
	}
      }
    }

    void compute_lmb(const CFGCollection *coll, int nb_way, int nb_index) {
      bool change = true;
      while (change) {
	
	change = false;
	
	for (CFGCollection::Iter cfg_iter(*coll); cfg_iter(); cfg_iter++) {
	  for (CFG::BlockIter block_iter = cfg_iter->blocks(); block_iter(); block_iter++) {

	    // ----- for each BB -----
	    if (block_iter->isBasic()) {
	      BasicBlock *bb = block_iter->toBasic();
	      Memory_block *mb = MB_STRUCT(bb);

	      // ----- for each row -----
	      
	      for (int index = 0; index < nb_index; index++) {

		// LMB OUT
		mb->lmb_out[index].clear();
		for (int suc_mb = 0; suc_mb < mb->suc.size(); suc_mb++) {
		  mb->lmb_out[index] =
		    get_set(vector_union(mb->lmb_out[index],
					 mb->suc[suc_mb]->lmb_in[index]));
		}

		// LMB IN
		std::vector<std::vector<Address>> oldout(mb->lmb_in[index]);
		mb->lmb_in[index].clear();

		std::vector<std::vector<Address>> lmb_out = get_set(mb->lmb_out[index]);
		if(lmb_out.size() == 0) {
		  // TODO: Need to modify this part to take into account empty gen
		  //if(mb->lmb_gen[index].size() != 0) {
		    mb->lmb_in[index].push_back(mb->lmb_gen[index]);
		    //}
		} else {
		  for (int tmp_lmb = 0; tmp_lmb < lmb_out.size(); tmp_lmb++) {
		    std::vector<Address> tmp_vect = mb->lmb_gen[index];
		    lmb_out[tmp_lmb] = get_set(lmb_LRU(tmp_vect, lmb_out[tmp_lmb], nb_way));
		    mb->lmb_in[index].push_back(lmb_out[tmp_lmb]);
		  }
		}

		mb->lmb_in[index] = get_set(mb->lmb_in[index]);

		// UPDATE END CONDITION
		if (oldout != mb->lmb_in[index]) {
		  change = true;
		}
	      }
	    }
	  }
	}
      }
      
    }
    
    void Initialize_RMB_LMBProcessor::processWorkSpace(WorkSpace *ws)
    {

      const CFGCollection *coll = INVOLVED_CFGS(ws);

     const otawa::hard::CacheConfiguration *cf = otawa::hard::CACHE_CONFIGURATION_FEATURE.get(ws); 
      const otawa::hard::Cache* cache = cf->instCache();

      int nb_index = cache->rowCount();
      int nb_way = cache->wayCount();

      // create structures
      create_structure(coll, nb_index, nb_way);
      
      // links structures
      link_structure(coll, nb_way, nb_index);

      // Initialize all structures
      initialize_structure(coll, nb_way, nb_index, cache);

      // RMB

      compute_rmb(coll, nb_way, nb_index);
      
      // LMB

      compute_lmb(coll, nb_way, nb_index);
      
      for (CFGCollection::Iter cfg_iter(*coll); cfg_iter(); cfg_iter++) {
	for (CFG::BlockIter block_iter = cfg_iter->blocks(); block_iter(); block_iter++) {
	  if (block_iter->isBasic()) {

	    BasicBlock *bb = block_iter->toBasic();
	    Memory_block *mb = MB_STRUCT(bb);
	    print_gen(mb,nb_index, nb_way);
	  }
	}
      }
      
    }
  
    // definition propriete
    /*
      Identifier<DAGHNode*> DAG_HNODE("otawa::initialize_RMB_LMB::DAG_HNODE");
    */
    Identifier<struct Memory_block *> MB_STRUCT("otawa::initialize_RMB_LMB::MB_STRUCT");

  } // namespace initialize_RMB_LMB
} // namespace otawa
