#include <otawa/app/Application.h>
#include <otawa/cfg/features.h>
#include <otawa/cache/features.h>
#include <otawa/script/Script.h>
#include <otawa/prog/WorkSpace.h>
#include <otawa/proc/DynProcessor.h>
#include <otawa/cfg/Dominance.h>
#include <otawa/proc/DynFeature.h>

#include <otawa/cache/cat2/CAT2Builder.h>
#include <otawa/cache/LBlockBuilder.h>


#include <otawa/hard/Cache.h>
#include <iostream>
#include <fstream>

//includes pour l'affichage du CFG
#include <otawa/display/CFGOutput.h>
#include <elm/io/OutFileStream.h>

#include "include/initialize_RMB_LMB.h"
#include "include/type.h"

using namespace std;
using namespace otawa; //comme import
using namespace otawa::initialize_RMB_LMB;


const otawa::hard::Cache *get_cache(WorkSpace *ws){ 

  const CFGCollection *coll = INVOLVED_CFGS(ws);

  for (CFGCollection::Iter cfg_iter(*coll); cfg_iter(); cfg_iter++)
    {

      for (CFG::BlockIter block_iter = cfg_iter->blocks(); block_iter(); block_iter++)
	{

	  if (block_iter->isBasic())
	    {

	      AllocArray<LBlock *> *lblock_array = BB_LBLOCKS(block_iter->toBasic());

	      if (lblock_array != NULL)
		{

		  for (int i = 0; i < lblock_array->count(); i++)
		    {

		      LBlock *lb = (*lblock_array)[i];

		      return lb->lblockset()->cache();
		    }
		}
	    }
	}
    }

  return NULL;
}

void print_mb(std::vector<std::vector<Address>> *multi, int nb_index, ofstream &file) {
  //file << "{";
  for(int i = 0; i < nb_index; i++) {

    std::vector<std::vector<Address>> tmp = get_set(multi[i]);
    file << "[" << i << "]";

    for(int j = 0; j < tmp.size(); j++) {
      file << " {";
      for(int k = 0; k < tmp[j].size(); k++) {
	file << tmp[j][k].page() << ":" << tmp[j][k].offset() << " ";
      }
      file << "}";
      
    }
    file << std::endl;
  }
  // file << "}";
}

int main(int argc, char **argv) {
  if (argc != 3) {
    fprintf(stderr, "usage: %s <binary> <cache>", argv[0]);
    exit(1);
  }
  WorkSpace *ws = NULL;
  PropList conf;
  Manager manager;
  NO_SYSTEM(conf) = true;
  TASK_ENTRY(conf) = "main";
  VERBOSE(conf) = true;
 
  CACHE_CONFIG_PATH(conf) = argv[2];
  StringBuffer buf;
  buf << argv[1] << "-decomp.c";
  elm::io::OutFileStream s(buf.toString());
  elm::io::Output out(s);
  conf.print(out);
  ws = manager.load(argv[1], conf);

  ws->require(DynFeature("otawa::initialize_RMB_LMB::INITIALIZE_RMB_LMB_FEATURE"), conf);

  std::ofstream file("ucb.txt");
  
  const CFGCollection *coll = INVOLVED_CFGS(ws);

  //get the number of index and way in the cache
  const hard::Cache *cache = get_cache(ws);
  int nb_index = cache->rowCount();
  int nb_way = cache->wayCount();

 
  
  for(CFGCollection::Iter cfg_iter(*coll); cfg_iter(); cfg_iter++) {
    for(CFG::BlockIter block_iter = cfg_iter->blocks(); block_iter(); block_iter++) {
      if(block_iter->isBasic()) {
	BasicBlock *bb = block_iter->toBasic();
	struct Memory_block *mb = MB_STRUCT(bb);

	file << "------------------------------------" << std::endl;
	file << "BB " << mb->block_id << std::endl;
	// print rmb in
	file << "RMB_IN" << std::endl;
	print_mb(mb->rmb_in, nb_index, file);
	file << "RMB_OUT" << std::endl;
	print_mb(mb->rmb_out, nb_index, file);
	file << "LMB_IN" << std::endl;
	print_mb(mb->lmb_in, nb_index, file);
	file << "LMB_OUT" << std::endl;
	print_mb(mb->lmb_out, nb_index, file);
	
	
	file << "------------------------------------" << std::endl;
      }
    }
  }

  file.close();
}
