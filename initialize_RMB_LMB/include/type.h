#ifndef TYPE_H
#define TYPE_H

#include <otawa/otawa.h>
#include <vector>
namespace otawa {
  namespace initialize_RMB_LMB {

    template <typename T>
    T vector_union(T v1, T v2) {
      T v3;
      for(auto iter_v = v1.begin(); iter_v != v1.end(); iter_v++) {
	v3.push_back(*iter_v);
      }
      for(auto iter_v = v2.begin(); iter_v != v2.end(); iter_v++) {
	v3.push_back(*iter_v);
      }
      return v3;
    }

    template <typename T>
    T get_set(T v1) {
      T v2;
      for(auto iter_v = v1.begin(); iter_v != v1.end(); iter_v++) {
	bool already_in = false;
	for(auto iter_v2 = v2.begin(); iter_v2 != v2.end(); iter_v2++) {
	  if(*iter_v2 == *iter_v) {
	    already_in = true;
	    break;
	  }
	}
	if(!already_in) {
	  v2.push_back(*iter_v);
	}
      }
      return v2;
    }
    
    struct Memory_block {
      int block_id;
      int nb_index;
      int nb_way;

      BasicBlock* bb_ptr;
      
      std::vector<struct Memory_block*> pred;
      std::vector<struct Memory_block*> suc;
      std::vector<Address> *lmb_gen;
      std::vector<Address> *rmb_gen;
      std::vector<std::vector<Address>> *rmb_in;
      std::vector<std::vector<Address>> *rmb_out;
      std::vector<std::vector<Address>> *lmb_in;
      std::vector<std::vector<Address>> *lmb_out;
      
      Memory_block(int nb_index, int nb_way, BasicBlock* bb) {

	this->bb_ptr = bb;
	this->nb_index = nb_index;
	this->nb_way = nb_way;
        this->rmb_in = new std::vector<std::vector<Address>>[nb_index];
	this->rmb_out = new std::vector<std::vector<Address>>[nb_index];
	this->lmb_in = new std::vector<std::vector<Address>>[nb_index];
	this->lmb_out = new std::vector<std::vector<Address>>[nb_index];
	this->lmb_gen = new std::vector<Address>[nb_index];
	this->rmb_gen = new std::vector<Address>[nb_index];
      }

      ~Memory_block() {
	delete this->rmb_in;
	delete this->rmb_out;
	delete this->lmb_in;
	delete this->lmb_out;
	delete this->lmb_gen;
	delete this->rmb_gen;
      }
      
    };
  }
}
	
#endif
