#ifndef INITIALIZE_RMB_LMB_H
#define INITIALIZE_RMB_LMB_H
#include <typeinfo>
#include <vector>
#include <algorithm>
#include <assert.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <functional>
#include "type.h"
#include <otawa/cfg.h>
#include <otawa/cfg/features.h>
#include <otawa/proc/ProcessorPlugin.h>
#include <otawa/cfg/Dominance.h>
#include <otawa/otawa.h>
namespace otawa { namespace initialize_RMB_LMB {
    class Initialize_RMB_LMBProcessor : public Processor {
    public:
      static p::declare reg;
      explicit Initialize_RMB_LMBProcessor(p::declare &r = reg);

    protected:
      void processWorkSpace(WorkSpace * /*ws*/) override;
      void configure(const PropList &props) override;
    };

    // declaration feature
    extern p::feature INITIALIZE_RMB_LMB_FEATURE;

    //declaration propriete 
    /*
      extern Identifier<DAGHNode*> DAG_HNODE;
    */
    extern Identifier<struct Memory_block*> MB_STRUCT;
    //extern Identifier<std::vector<int>> UCB;
    
  } }
#endif
