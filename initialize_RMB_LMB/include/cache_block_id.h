#ifndef CACHE_BLOCK_ID_H
#define CACHE_BLOCK_ID_H
#include <typeinfo>
#include <vector>
#include <algorithm>
#include <assert.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <functional>

#include <otawa/cfg.h>
#include <otawa/cfg/features.h>
#include <otawa/proc/ProcessorPlugin.h>
#include <otawa/cfg/Dominance.h>
#include <otawa/otawa.h>
namespace otawa { namespace cache_block_id {
class Cache_block_idProcessor : public Processor {
public:
	static p::declare reg;
	explicit Cache_block_idProcessor(p::declare &r = reg);

protected:
	void processWorkSpace(WorkSpace * /*ws*/) override;
	void configure(const PropList &props) override;
};

// declaration feature
extern p::feature CACHE_BLOCK_ID_FEATURE;

//declaration propriete 

		extern Identifier<Address> CB_ID;

} }
#endif
