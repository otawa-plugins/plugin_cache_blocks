#include <otawa/app/Application.h>
#include <otawa/cfg/features.h>
#include <otawa/script/Script.h>
#include <otawa/prog/WorkSpace.h>
#include <otawa/proc/DynProcessor.h>
#include <otawa/cfg/Dominance.h>
#include <otawa/proc/DynFeature.h>

//includes pour l'affichage du CFG
#include <otawa/display/CFGOutput.h>
#include <elm/io/OutFileStream.h>

#include "include/ucb_otawa.h"

#include <multiset.h>

using namespace otawa; //comme import
using namespace otawa::ucb_otawa;
using namespace Mathset;

int main(int argc, char **argv) {
  if (argc != 2) {
    fprintf(stderr, "usage: %s <binary> ", argv[0]);
    exit(1);
  }
  WorkSpace *ws = NULL;
  PropList conf;
  Manager manager;
  NO_SYSTEM(conf) = true;
  TASK_ENTRY(conf) = "main";
  VERBOSE(conf) = true;
 
  CACHE_CONFIG_PATH(conf) = "./cache.xml";
  StringBuffer buf;
  buf << argv[1] << "-decomp.c";
  elm::io::OutFileStream s(buf.toString());
  elm::io::Output out(s);
  conf.print(out);
  ws = manager.load(argv[1], conf);

  ws->require(DynFeature("otawa::ucb_otawa::UCB_OTAWA_FEATURE"), conf);

  Multiset<Multiset<int>> ucb = UCB_OTAWA(ws);
  
  std::vector<struct Element<Multiset<int>>> vect = ucb.get_multiset();

  for(int i = 0; i < vect.size(); i++) {
    for(int k = 0; k < vect[i].counter; k++) {
      for(int j = 0; j < vect[i].elem.size(); j++) {
	std::cout << vect[i].elem[j] << " ";
      }
    
      std::cout << endl;
    }
  }
	
}
