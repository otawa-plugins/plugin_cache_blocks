#include "include/ucb_otawa.h"
#include <otawa/cfg/Loop.h>
#include <multiset.h>

using namespace Mathset;

namespace otawa {
  namespace ucb_otawa {

    // Declaration du plugin

    class Plugin : public ProcessorPlugin { 
    public:
      Plugin() : ProcessorPlugin(
                                 "otawa::ucb_otawa",
                                 Version(1, 0, 0),
                                 OTAWA_PROC_VERSION
                                 ) {}
    };

    otawa::ucb_otawa::Plugin ucb_otawa_plugin;
    ELM_PLUGIN(ucb_otawa_plugin, OTAWA_PROC_HOOK);

    p::declare Ucb_otawaProcessor::reg = p::init(
                                                 "otawa::ucb_otawa::Ucb_otawaProcessor",
                                                 Version(1, 0, 0)
                                                 )
      .require(COLLECTED_CFG_FEATURE)
      .require(LOOP_INFO_FEATURE)
      .require(ICACHE_CATEGORY2_FEATURE)
      .require(cache_block_id::CACHE_BLOCK_ID_FEATURE)
      .require(cache::COLLECTED_LBLOCKS_FEATURE)
      .require(hard::CACHE_CONFIGURATION_FEATURE)
      .provide(UCB_OTAWA_FEATURE);
    // definition feature
    p::feature UCB_OTAWA_FEATURE(
                                 "otawa::ucb_otawa::UCB_OTAWA_FEATURE",
                                 new Maker<Ucb_otawaProcessor>()
                                 );

    Ucb_otawaProcessor::Ucb_otawaProcessor(p::declare &r) : Processor(r) {}

    void Ucb_otawaProcessor::configure(const PropList &props) {
      Processor::configure(props);
    }


    bool is_in_loop(struct preemption_node* node, int loop_id) {
      if(node->loop_id == loop_id) {
	return true;
      }

      Loop* parent_loop = node->loop_of;
      
      while(parent_loop != NULL) {
	if(parent_loop->header()->id() == loop_id) {
	  return true;
	}
	parent_loop = parent_loop->parent();
      }
      
      return false;
    }
    

    void insert_fct(struct preemption_graph* graph, int top_loop_id) {
      for(int i = 0; i < graph->nodes.size(); i++) {

	if(graph->nodes[i]->is_top) {
	  graph->nodes[i]->loop_id = top_loop_id;
	}
	
	if(graph->nodes[i]->synth) {
	  
	  insert_fct(graph->nodes[i]->fct, graph->nodes[i]->loop_id);

	  
	  std::vector<struct preemption_node*> pred = graph->nodes[i]->pred;
	  std::vector<struct preemption_node*> succ = graph->nodes[i]->succ;
	  if(pred.size() != 0 || succ.size() != 0) {
	    struct preemption_node* entry = graph->nodes[i]->fct->get_entry();
	    struct preemption_node* exit = graph->nodes[i]->fct->get_exit();
	  
	    // delete old reference to BB;
	    for(int j = 0; j < pred.size(); j++) {
	      for(int k = 0; k < pred[j]->succ.size(); k++) {
		if(pred[j]->succ[k] == graph->nodes[i]) {
		  pred[j]->succ.erase(pred[j]->succ.begin() + k);
		  break;
		}
	      }
	    }
	    
	    // delete old reference to BB;
	    for(int j = 0; j < succ.size(); j++) {
	      for(int k = 0; k < succ[j]->pred.size(); k++) {
		if(succ[j]->pred[k] == graph->nodes[i]) {
		  succ[j]->pred.erase(succ[j]->pred.begin() + k);
		  break;
		}
	      }
	    }
	  
	    //pred
	    for(int j = 0; j < pred.size(); j++) {
	      for(int k = 0; k < entry->succ.size(); k++) {
		pred[j]->succ.push_back(entry->succ[k]);
		entry->succ[k]->pred.push_back(pred[j]);
	      }
	    }

	    //succ
	    for(int j = 0; j < succ.size(); j++) {
	      for(int k = 0; k < exit->pred.size(); k++) {
		exit->pred[k]->succ.push_back(succ[j]);
		succ[j]->pred.push_back(exit->pred[k]);
	      }
	    }

	    //remove link to synth block
	    graph->nodes[i]->pred.clear();
	    graph->nodes[i]->succ.clear();
	  }
	}
	
      }
    }
    
    void insert_lb(struct preemption_graph* graph, int& id) {
      
      for(int i = 0; i < graph->nodes.size(); i++) {

	if(graph->nodes[i]->synth) {
	  insert_lb(graph->nodes[i]->fct, id);
	}
	
	if(graph->nodes[i]->bb) {
	  
	  std::vector<struct preemption_node*> pred = graph->nodes[i]->pred;
	  std::vector<struct preemption_node*> succ = graph->nodes[i]->succ;

	  BasicBlock* bb = graph->nodes[i]->basic_block;
	  
	  AllocArray<LBlock *> *lblock_array = BB_LBLOCKS(bb);

	  // delete old reference to predecessor of BB;
	  for(int j = 0; j < pred.size(); j++) {
	    for(int k = 0; k < pred[j]->succ.size(); k++) {
	      if(pred[j]->succ[k] == graph->nodes[i]) {
		pred[j]->succ.erase(pred[j]->succ.begin() + k);
		k--;
	      }
	    }
	  }
	    
	  // delete old reference to successor of BB;
	  for(int j = 0; j < succ.size(); j++) {
	    for(int k = 0; k < succ[j]->pred.size(); k++) {
	      if(succ[j]->pred[k] == graph->nodes[i]) {
		succ[j]->pred.erase(succ[j]->pred.begin() + k);
		k--;
	      }
	    }
	  }

	  graph->nodes[i]->pred.clear();
	  graph->nodes[i]->succ.clear();

	  struct preemption_node *tmp;
	  // link lb
	  for (int i_lb = 0; i_lb < lblock_array->count(); i_lb++) {
	    LBlock *lb = (*lblock_array)[i_lb];
	    category_t status = cache::CATEGORY(lb);

	    tmp = new struct preemption_node;

	    if(graph->nodes[i]->is_loop_header) {
	      tmp->is_loop_header = true;
	    } else {
	      tmp->is_loop_header = false;
	    }

	    tmp->loop_id = graph->nodes[i]->loop_id;
	    tmp->loop_of = Loop::of(graph->nodes[i]->basic_block);
	    tmp->is_top = graph->nodes[i]->is_top;
	    
	    tmp->status = status;
	    tmp->bb = false;
	    tmp->lb = true;
	    tmp->cb_id = otawa::cache_block_id::CB_ID(lb);
	    tmp->id = id;
	    tmp->line = lb->lblockset()->line();
	    id++;

	    //pred
	    for(int j = 0; j < pred.size(); j++) {
	      pred[j]->succ.push_back(tmp);
	      tmp->pred.push_back(pred[j]);
	    }
	    
	    graph->nodes.push_back(tmp);
	    pred.clear();
	    pred.push_back(tmp);
	    
	  }
	  
	  //succ
	  for(int j = 0; j < succ.size(); j++) {
	    tmp->succ.push_back(succ[j]);
	    succ[j]->pred.push_back(tmp);
	  }
	  
	}
	
      }
    }
   
    void link_all_nodes_for_real(struct preemption_graph* graph) {
      
      for(int i = 0; i < graph->nodes.size(); i++) {
	
	if(graph->nodes[i]->synth) {
	  link_all_nodes_for_real(graph->nodes[i]->fct);
	}
	
	for(int j = 0; j < graph->nodes[i]->succ_id.size(); j++) {
	  graph->nodes[i]->succ.push_back(graph->get_node_by_id( graph->nodes[i]->succ_id[j]));
	}

	for(int j = 0; j < graph->nodes[i]->pred_id.size(); j++) {
	  graph->nodes[i]->pred.push_back(graph->get_node_by_id( graph->nodes[i]->pred_id[j]));
	}
	
      }
      
    }
    
    void create_preemption_nodes(struct preemption_graph* graph, Block *b, const CFG* cfg) {

      //creation du noeud
      struct preemption_node* tmp = new preemption_node;
     
      //copier l'id
      tmp->id_b = b->id();

      //Loop
      tmp->loop_id = Loop::of(b)->header()->id();
      tmp->is_top = Loop::of(b)->isTop();
      tmp->is_loop_header = (tmp->id_b == tmp->loop_id);
	
      bool found = false;
      for(int i = 0; i < graph->nodes.size(); i++) {
        if(graph->nodes[i]->id_b == tmp->id_b) {
          found = true;
          break;
        }
      }

      if(found) {

        delete tmp;

      } else {

        //ajout du noeud au graphe
        graph->nodes.push_back(tmp);


        if(b->isBasic()) {
          tmp->bb = true;
	  tmp->basic_block = b->toBasic();
        }

        // construction des noeuds de la fonction
        if(b->isSynth()) {
          tmp->synth = true;
	  tmp->fct = new struct preemption_graph;
          create_preemption_nodes(tmp->fct, b->toSynth()->callee()->entry(), b->toSynth()->callee());
        }

	//predécesseurs
	for (Block::EdgeIter iter_pred(b->ins()); iter_pred(); iter_pred++) {
	  Edge *edge = *iter_pred;
          Block *source = edge->source();
	  tmp->pred_id.push_back(source->id());
	}

	//successeurs
        for (Block::EdgeIter iter_suc(b->outs()); iter_suc(); iter_suc++) {

          Edge *edge = *iter_suc;
          Block *target = edge->target();
          tmp->succ_id.push_back(target->id());

          //recurence
          create_preemption_nodes(graph, target, cfg);
        }

      }
    }

    bool mark_always(struct preemption_node* node, int lb_id, Address cb_id, std::vector<int> visited) {

      
      
      //If not visited yet
      for(int i = 0; i < visited.size(); i++) {
	if(visited[i] == node->id) {
	  Address bug_add(0, 33056);
	  if(cb_id == bug_add) {
	    //   std::cout << "node id:" << node->id << std::endl;
	  }
	  if(cb_id == bug_add) {
	    //   std::cout << "already visit" << std::endl;
	  }
	  //we return false because we do not encounter any block from the same CBL on this path
	  return false;
	}
      }

     
      if(node->id == 0) {
	//	std::cout <<"############################################## HERE ##############" << std::endl;
      }

      Address bug_add(0, 33056);
      if(cb_id == bug_add) {

	//	std::cout << node->id << " " << node->cb_id.offset() << " " << cb_id.offset() << std::endl;
      }
      if(node->cb_id == cb_id) {
	Address bug_add(0, 33056);
	if(cb_id == bug_add) {
	  //	std::cout << "node id:" << node->id << std::endl;
	}
	if(cb_id == bug_add) {
	  //std::cout << "the node is the same" << std::endl;
	}
	//If the node is from the same CBL we return true
	//It is the first block from this CBL encounter
	return true;
	
      } else {

	//we said we visit this block
	visited.push_back(node->id);
	
	bool not_reach = true;
	//we visit predecessors (and build path)
	for(int i = 0; i < node->pred.size(); i++) {
	  if(mark_always(node->pred[i], lb_id, cb_id, visited)) {
	    not_reach = false;
	  }
	}
	Address bug_add(0, 33056);
	if(cb_id == bug_add) {
	  //	std::cout << "node id:" << node->id << std::endl;
	}

	//if this block is contains in a hit path then we cachedthe CBL in it 
	if(not_reach) {
	  if(cb_id == bug_add) {
	    //	std::cout << "not reach" << std::endl;
	  }
	  return false;
	} else {
	  if(cb_id == bug_add) {
	    //	std::cout << "reached" << std::endl;
	  }
	  node->cached.push_back(cb_id);
	  return true;
	}
      }
    }

    bool mark_hit(struct preemption_node* node, int lb_id, Address cb_id, std::vector<int> visited, int loop_id) {

      
      // if the node is not in the loop then it can't be in path that provoke an hit
      if(is_in_loop(node, loop_id)) {
	return false;
      }
	
      //If not visited yet
      for(int i = 0; i < visited.size(); i++) {
	if(visited[i] == node->id) {
	  //we return false because we do not encounter any block from the same CBL on this path
	  return false;
	}
      }

      
      if(node->cb_id == cb_id) {
	//If the node is from the same CBL we return true
	//It is the first block from this CBL encounter
	return true;
	
      } else {

	//we said we visit this block
	visited.push_back(node->id);
	
	bool not_reach = true;
	//we visit predecessors (and build path)
	for(int i = 0; i < node->pred.size(); i++) {
	  if(mark_hit(node->pred[i], lb_id, cb_id, visited, loop_id)) {
	    not_reach = false;
	  }
	}

	//if this block is contains in a hit path then we cachedthe CBL in it 
	if(not_reach) {
	  return false;
	} else {
	  node->cached.push_back(cb_id);
	  return true;
	}
      }
    }

    bool mark_miss(struct preemption_node* node, int lb_id, Address cb_id, std::vector<int> visited, int loop_id) {

      // if the node is not in the loop then it can't be in path that provoke an hit
      if(!is_in_loop(node, loop_id)) {
	return false;
      }
	
      //If not visited yet
      for(int i = 0; i < visited.size(); i++) {
	if(visited[i] == node->id) {
	  //we return false because we do not encounter any block from the same CBL on this path
	  return false;
	}
      }

      
      if(node->cb_id == cb_id) {
	//If the node is from the same CBL we return true
	//It is the first block from this CBL encounter
	return true;
	
      } else {

	//we said we visit this block
	visited.push_back(node->id);
	
	bool not_reach = true;
	//we visit predecessors (and build path)
	for(int i = 0; i < node->pred.size(); i++) {
	  if(mark_miss(node->pred[i], lb_id, cb_id, visited, loop_id)) {
	    not_reach = false;
	  }
	}

	//if this block is contains in a hit path then we cachedthe CBL in it 
	if(not_reach) {
	  return false;
	} else {
	  node->cached.push_back(cb_id);
	  return true;
	}
      }
    }
    
    void compute_useful_cache_block(struct preemption_node* node) {
      
      node->cached.push_back(node->cb_id);
      std::vector<int> visited;
      visited.clear();
      switch(node->status) {
        
      case ALWAYS_HIT:
	std::cout << " ALWAYS HIT ---------------------------------" << std::endl;
	for(int i = 0; i < node->pred.size(); i++) {
	  mark_always(node->pred[i], node->id, node->cb_id, visited);
	}
	break;
	
      case FIRST_MISS:
	std::cout << " FIRST MISS ---------------------------------" << std::endl;
	for(int i = 0; i < node->pred.size(); i++) {
	  mark_miss(node->pred[i], node->id, node->cb_id, visited, node->loop_id);
	}
	break;
	  
      case FIRST_HIT:
	std::cout << " FIRST HIT ---------------------------------" << std::endl;
	for(int i = 0; i < node->pred.size(); i++) {
	  mark_hit(node->pred[i], node->id, node->cb_id, visited, node->loop_id);
	}
	break;
	  
      case ALWAYS_MISS:
	break;
	
      default:
	break;
      }
    }
    
    void compute_useful_cache_blocks(struct preemption_graph* graph) {

      
      for(int i = 0; i < graph->nodes.size(); i++) {
	if(graph->nodes[i]->lb) {
	  compute_useful_cache_block(graph->nodes[i]);
	}
	if(graph->nodes[i]->synth) {
	  compute_useful_cache_blocks(graph->nodes[i]->fct);
	}
      }
      
    }

    void delete_doublon(struct preemption_graph* graph) {
      for(int i = 0; i < graph->nodes.size(); i++) {

	for(int j = 0; j < graph->nodes[i]->cached.size(); j++) {
	  for(int k = j + 1; k < graph->nodes[i]->cached.size(); k++) {
	    if(graph->nodes[i]->cached[j] == graph->nodes[i]->cached[k]) {
	      graph->nodes[i]->cached.erase(graph->nodes[i]->cached.begin() + k);
	      k--;
	    }
	  }
	}
	  
	if(graph->nodes[i]->synth) {
	  delete_doublon(graph->nodes[i]->fct);
	}
      }
    }
    
    void compute_line_useful_cache_blocks(struct preemption_graph* graph, const hard::Cache *cache, Multiset<Multiset<int>> &l) {
      for(int i = 0; i < graph->nodes.size(); i++) {
	if(graph->nodes[i]->lb) {
	  std::cout << "LB(" << graph->nodes[i]->id << ") ";
	  for(int j = 0; j < graph->nodes[i]->cached.size(); j++) {
	    graph->nodes[i]->useful.push_back(cache->line(graph->nodes[i]->cached[j]));
	    std::cout << graph->nodes[i]->useful[j] << " ";
	  }
	  std::cout << std::endl;
	  Multiset<int> tmp_m;
	  for(int k = 0; k < graph->nodes[i]->useful.size(); k++) {
	    tmp_m.add_elem(graph->nodes[i]->useful[k]);
	  }
	  l.add_elem(tmp_m);
	}
	if(graph->nodes[i]->synth) {
	  compute_line_useful_cache_blocks(graph->nodes[i]->fct, cache, l);
	}
      }
    }


    void explo(struct preemption_graph* graph) {
      std::cout << "---------------------------START-------------------------" <<std::endl;
      
      for(int i = 0; i < graph->nodes.size(); i++) {
	
	std:: cout << "----" << std::endl;
	
	if(graph->nodes[i]->bb) {
	  //std::cout << "BB(" <<  graph->nodes[i]->id_b << ")" << std::endl;
	} else {
	  if(graph->nodes[i]->synth) {
	    // std::cout << "S(" <<  graph->nodes[i]->id_b << ")" << std::endl;
	    explo(graph->nodes[i]->fct);
	  } else {
	    if(graph->nodes[i]->lb) {
	      std::cout << "LB(" << graph->nodes[i]->id << ") " << graph->nodes[i]->cb_id.offset() << std::endl;
	    } else {
	      // std::cout << "V(" << graph->nodes[i]->id_b << ")" << std::endl;
	    }
	  }
	}

	if(graph->nodes[i]->lb) {
	std::cout << "pred: ";
        for(int j = 0; j < graph->nodes[i]->pred.size(); j++) {
	  if(graph->nodes[i]->pred[j]->lb) {
	    std::cout << "lb(" << graph->nodes[i]->pred[j]->id << ") ";
	  } else {
	    if(graph->nodes[i]->pred[j]->bb) {
	      std::cout << "id_bb(" << graph->nodes[i]->pred[j]->id_b << ") ";
	    } else {
	      if(graph->nodes[i]->pred[j]->synth) {
		std::cout << "id_s(" << graph->nodes[i]->pred[j]->id_b << ") ";
	      } else {
		std::cout << "id_v(" << graph->nodes[i]->pred[j]->id_b << ") ";
	      }
	    }
	  }
	}
	std::cout << std::endl;

	std::cout << "succ: ";
        for(int j = 0; j < graph->nodes[i]->succ.size(); j++) {
	  if(graph->nodes[i]->succ[j]->lb) {
	    std::cout << "lb(" << graph->nodes[i]->succ[j]->id << ") ";
	  } else {
	    if(graph->nodes[i]->succ[j]->bb) {
	      std::cout << "id_bb(" << graph->nodes[i]->succ[j]->id_b << ") ";
	    } else {
	      if(graph->nodes[i]->succ[j]->synth) {
		std::cout << "id_s(" << graph->nodes[i]->succ[j]->id_b << ") ";
	      } else {
		std::cout << "id_v(" << graph->nodes[i]->succ[j]->id_b << ") ";
	      }
	    }
	  }
	}
	std::cout << std::endl;
	}
	
	std::cout << "----" << std::endl;
	
      }
      
      std::cout << "---------------------------END-------------------------" <<std::endl;
    }
    
    void Ucb_otawaProcessor::processWorkSpace(WorkSpace *ws) {
      
      const CFGCollection *coll = INVOLVED_CFGS(ws);
      struct preemption_graph *graph = new struct preemption_graph;
      
      create_preemption_nodes(graph, coll->entry()->entry(), coll->entry());
      link_all_nodes_for_real(graph);
      int id = 0;

      
      insert_fct(graph, graph->nodes[0]->loop_id);
      insert_lb(graph, id);
      compute_useful_cache_blocks(graph);
      delete_doublon(graph);
      
      const otawa::hard::CacheConfiguration *cf = otawa::hard::CACHE_CONFIGURATION_FEATURE.get(ws); 
      const otawa::hard::Cache* cache = cf->instCache();

      // LOCAL UCB

      Multiset<Multiset<int>> l;
      compute_line_useful_cache_blocks(graph, cache, l);
      l.to_set();
      UCB_OTAWA(ws) = l;

      // TOTAL UCB
      
      Multiset<int> l_t;
      for(int i = 0; i < l.size(); i++) {
	l_t = math_multiset_fusion(l_t, l[i]);
      }

      TOTAL_UCB(ws) = l_t;
      // explo(graph);
       
    }

    
    
    // definition propriete 
    
    Identifier<Multiset<Multiset<int>>> UCB_OTAWA("otawa::ucb_otawa::UCB_OTAWA");
    Identifier<Multiset<int>> TOTAL_UCB("otawa::ucb_otawa::TOTAL_UCB");

  }
}
