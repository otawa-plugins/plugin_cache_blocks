#ifndef UCB_OTAWA_H
#define UCB_OTAWA_H
#include <iostream>
#include <typeinfo>
#include <vector>
#include <algorithm>
#include <assert.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <functional>

#include <otawa/hard/CacheConfiguration.h>
#include <otawa/cfg.h>
#include <otawa/cfg/features.h>
#include <otawa/proc/ProcessorPlugin.h>
#include <otawa/cfg/Dominance.h>
#include <otawa/otawa.h>

#include <otawa/cfg/CFG.h>
#include <otawa/cache/features.h>
#include <otawa/cache/cat2/CAT2Builder.h>
#include <otawa/cache/LBlockBuilder.h>

#include "../../cache_block_id/include/cache_block_id.h"
#include <multiset.h>
#include <otawa/cfg/Loop.h>

namespace otawa {
  namespace ucb_otawa {

    //struct preemption_graph;

    struct preemption_node {
      
      //Construction des noeuds
      int id_b;
      std::vector<int> pred_id;
      std::vector<int> succ_id;

      //insertion des fonctions
      bool synth;
      struct preemption_graph* fct;

      //insertion des LBs
      bool bb;
      BasicBlock *basic_block;
      
      // Loop informations
      bool is_top;
      Loop* loop_of;
      bool is_loop_header;
      int loop_id;
      
      //----------------------------------------------------------

      std::vector<struct preemption_node*> pred;
      std::vector<struct preemption_node*> succ;
      std::vector<int> useful; //line of cache block used
      std::vector<Address> cached; //id of useful block


      // --- CACHE BLOCK INFO ---
      Address cb_id;
      int line; // cache line

      // --- LB INFO ---
      bool lb;
      int id;
      category_t status;
      
      // --- ARC INFO ---
      bool arc;

      preemption_node() {
	is_top = true;
      loop_of = NULL;
     is_loop_header = false;
      loop_id = -1;
        synth = false;
        bb = false;
        lb = false;
        id_b = 0;
        id = 0;
        line = 0;
	status = NOT_CLASSIFIED;
	fct = NULL;
      }

      ~preemption_node() {
	if(fct != NULL) {
	delete fct;
	}
	
      }
    };

    struct preemption_graph {

      std::vector<struct preemption_node *> nodes;

      ~preemption_graph() {
        for(int i = 0; i < nodes.size(); i++) {
          delete nodes[i];
        }
      }

      struct preemption_node* get_node_by_id(int id) {
	for(int i = 0; i < nodes.size(); i++) {
	  if(nodes[i]->id_b == id) {
	    return nodes[i];
	  }
	}
      }

      struct preemption_node* get_entry() {
	// for(int i = 0; i < nodes.size(); i++) {
	//   if(nodes[i]->pred.size() == 0) {
	//     return nodes[i];
	//   }
	// }
	return nodes[0];
      }
      
      struct preemption_node* get_exit() {
	for(int i = 0; i < nodes.size(); i++) {
	  if(nodes[i]->succ.size() == 0) {
	    return nodes[i];
	  }
	}
      }
    };

    class Ucb_otawaProcessor : public Processor {
    public:
      static p::declare reg;
      explicit Ucb_otawaProcessor(p::declare &r = reg);

    protected:
      void processWorkSpace(WorkSpace * /*ws*/) override;
      void configure(const PropList &props) override;
    };

    // declaration feature
    extern p::feature UCB_OTAWA_FEATURE;

    //declaration propriete 
    
    extern Identifier<Mathset::Multiset<Mathset::Multiset<int>>> UCB_OTAWA;
    extern Identifier<Mathset::Multiset<int>> TOTAL_UCB;
  }
}
#endif
