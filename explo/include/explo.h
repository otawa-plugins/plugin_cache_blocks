#ifndef EXPLO_H
#define EXPLO_H
#include <typeinfo>
#include <vector>
#include <algorithm>
#include <assert.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <functional>

#include <otawa/cfg.h>
#include <otawa/cfg/features.h>
#include <otawa/proc/ProcessorPlugin.h>
#include <otawa/cfg/Dominance.h>
#include <otawa/otawa.h>
namespace otawa { namespace explo {
class ExploProcessor : public Processor {
public:
	static p::declare reg;
	explicit ExploProcessor(p::declare &r = reg);

protected:
	void processWorkSpace(WorkSpace * /*ws*/) override;
	void configure(const PropList &props) override;
};

// declaration feature
extern p::feature EXPLO_FEATURE;

//declaration propriete 
/*
extern Identifier<DAGHNode*> DAG_HNODE;
*/
} }
#endif