//#include <iostream>
#include <otawa/app/Application.h>
#include <otawa/cfg/features.h>
#include <otawa/script/Script.h>
#include <otawa/prog/WorkSpace.h>
#include <otawa/proc/DynProcessor.h>
#include <otawa/cfg/Dominance.h>
#include <otawa/proc/DynFeature.h>

//includes pour l'affichage du CFG
#include <otawa/display/CFGOutput.h>
#include <elm/io/OutFileStream.h>
// includes cache
#include <otawa/cache/cat2/features.h>
//#include <otawa/icat3/features.h>
// includes wcet
#include <otawa/ipet/features.h>
#include <otawa/ipet/FlowFactLoader.h>
#include <otawa/flowfact/features.h>
// include plugin explo
#include "include/explo.h"
// processors includes 
#include <otawa/hard/features.h>
#include <otawa/hard/Processor.h>
// edge time processors
#include <otawa/etime/features.h>
#include <otawa/tsim/BBTimeSimulator.h>
#include <otawa/sim/Simulator.h>
//#include <otawa/icat3/features.h>

using namespace otawa; //comme import
using namespace otawa::explo;

int main(int argc, char **argv) {

  if(argc != 3) {
    std::cout << "Error number of argument. Usage:" << argv[0] << " <binary> <flow fact>" << std::endl;
  }
  
  
  WorkSpace *ws = NULL;
  //WorkSpace *ws_trivial = NULL;
  PropList conf;
  //PropList conf_trivial;
  Manager manager;
  //Manager manager_trivial;
  NO_SYSTEM(conf) = true;
  TASK_ENTRY(conf) = "main";
  VERBOSE(conf) = true;

  // NO_SYSTEM(conf_trivial) = true;
  // TASK_ENTRY(conf_trivial) = "main";
  // VERBOSE(conf_trivial) = true;

  // Flow fact
  std::cout << "-------------------- Configuration paths ----------------------" << std::endl;
  FLOW_FACTS_PATH(conf) = argv[2];
  //FLOW_FACTS_PATH(conf_trivial) = argv[2];
  
  // Platform description
  CACHE_CONFIG_PATH(conf) = "xmc4500/cache.xml";
  PROCESSOR_PATH(conf)  = "xmc4500/pipeline.xml";
  //MEMORY_PATH(conf) = "xmc4500/memory.xml";
  
  // Generic processor
  //PROCESSOR_PATH(conf)  = "/home/fabien/cristal/otawa/share/Otawa/scripts/generic/pipeline.xml";
  
  std::cout << "----------------------------Workspace building----------------------------------" << std::endl;
  ws = manager.load(argv[1], conf);
  //ws_trivial = manager_trivial.load(argv[1], conf_trivial);
  // CFG Building
  std::cout << "----------------------------run virtualizer--------------------------------------" << std::endl;
   ws->run("otawa::Virtualizer", conf);
   //ws_trivial->run("otawa::Virtualizer", conf_trivial);
  std::cout << "-------------------------------cache configuration-----------------------------------------" << std::endl;
  // ws->require(otawa::ICACHE_ONLY_CONSTRAINT2_FEATURE , conf);
  std::cout << "-------------------------------edge time-----------------------------------------" << std::endl;
  otawa::etime::RECORD_TIME(conf) = true;
  
  //ws->require(otawa::ICACHE_ONLY_CONSTRAINT2_FEATURE, conf);
  //ws->require(otawa::ICACHE_CONSTRAINT2_FEATURE , conf);
  ws->require(otawa::etime::EDGE_TIME_FEATURE, conf);
  
  std::cout << "-------------------------------wcet----------------------------------------------" << std::endl;
  ws->require(otawa::ipet::WCET_FEATURE, conf);
  
  ws->require(DynFeature("otawa::explo::EXPLO_FEATURE"), conf);
  
 

  //ws_trivial->require(otawa::ipet::WCET_FEATURE, conf_trivial);
  std::cout << "WCET:" << ipet::WCET(ws) << std::endl;
  // std::cout << "WCET:" << ipet::WCET(ws_trivial) << std::endl;
  return 0;
}
