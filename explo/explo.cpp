#include "include/explo.h"

#include <otawa/prog/Inst.h>
#include <otawa/proc/DynFeature.h>
#include <otawa/hard/CacheConfiguration.h>
#include <otawa/cache/features.h>
#include <otawa/sim/TrivialSimulator.h>
#include <otawa/cfg/CFG.h>
#include <otawa/cache/features.h>
#include <otawa/cache/cat2/CAT2Builder.h>
#include <otawa/cache/LBlockBuilder.h>

#include <otawa/ipet/features.h>
#include <otawa/ipet/FlowFactLoader.h>

#include <otawa/flowfact/features.h>
#include <otawa/cfg/Loop.h>
#include <otawa/etime/features.h>

namespace otawa {
  namespace explo {

    // Declaration du plugin

    class Plugin : public ProcessorPlugin { 
    public:
      Plugin() : ProcessorPlugin("otawa::explo", Version(1, 0, 0), OTAWA_PROC_VERSION) {}
    };

    otawa::explo::Plugin explo_plugin;
    ELM_PLUGIN(explo_plugin, OTAWA_PROC_HOOK);

    p::declare ExploProcessor::reg = p::init("otawa::explo::ExploProcessor", Version(1, 0, 0))
      .require(COLLECTED_CFG_FEATURE)
      .require(LOOP_INFO_FEATURE)
      .require(etime::EDGE_TIME_FEATURE)
      .require(MKFF_PRESERVATION_FEATURE)
      .require(ipet::FLOW_FACTS_FEATURE)
      .require(cache::COLLECTED_LBLOCKS_FEATURE)
      .require(hard::CACHE_CONFIGURATION_FEATURE)
      .require(ipet::BB_TIME_FEATURE)
      .require(otawa::ipet::WCET_FEATURE)
      .provide(EXPLO_FEATURE);
    // definition feature
    p::feature EXPLO_FEATURE("otawa::explo::EXPLO_FEATURE", new Maker<ExploProcessor>());

    ExploProcessor::ExploProcessor(p::declare &r) : Processor(r) {}

    void ExploProcessor::configure(const PropList &props) {
      Processor::configure(props);

    }

    // unsigned long time_lblock(LBlock* lb) {
    //   BasicBlock* bb = lb->bb();
    //   std::cout << " -- LB -- " << std::endl;
    //   InstIter* iter = lb->instruction();
    //   // for(iter;) {
    //   //std::cout << "instruction:" << &(instructions[i]) << std::endl;
    // 	//}
    //   std::cout << " -------- " << std::endl;
    //   //INSTRUCTION_TIME();
    // }
    void compute_trivial_lblocks(BasicBlock *bb) {
      
      auto iter = bb->begin();
      AllocArray<LBlock *> *lblock_array = BB_LBLOCKS(bb);
      for (int i = 0; i < lblock_array->count(); i++) {
      	LBlock *lb = (*lblock_array)[i];
	unsigned long time_lblock = 0;
      	for(int j = 0; j < lb->countInsts(); j++) {
	  unsigned long cost_instruction = otawa::sim::INSTRUCTION_TIME(*iter);
	  if(cost_instruction <= 0) {
	    //std::cout << "INSTR_TRVL" << std::endl;
	    cost_instruction = 5;
	  } else {
	    //std::cout << "INSTR_V:" << cost_instruction << std::endl;
	  }

	  time_lblock += cost_instruction;
	  
      	  iter++;
      	}
	//std::cout << "lblock time:" << time_lblock << std::endl;
      }
      
    }

    unsigned long time_bblock(BasicBlock* bb) {
      std::cout << " -- BB -- " << std::endl;
      for(auto i : *bb) {
	//std::cout << "instruction:" << ipet::TIME(i) << std::endl;
      }
      std::cout << " -------- " << std::endl;
      
    }
    
    int get_exit_id_of_cfg(CFG *cfg) {
      for(CFG::BlockIter block_iter = cfg->blocks(); block_iter(); block_iter++) {
	Block * b = *block_iter;
	bool found = true;
        for(
	    Block::EdgeIter iter_suc(b->outs());
	    iter_suc();
	    iter_suc++) {
	  found = false;
	  break;
	}
	if(found) {
	  return b->id();
	}
      }
      return -1;
      
    }
    
    std::vector<Block*> get_last_blocks(CFG *cfg) {
      std::vector<Block*> tmp_v;
      int id_last = get_exit_id_of_cfg(cfg);
      if(id_last == -1) {
	return tmp_v;
      }
      for(CFG::BlockIter block_iter = cfg->blocks(); block_iter(); block_iter++) {
	Block * b = *block_iter;
        
        for(
	    Block::EdgeIter iter_suc(b->outs());
	    iter_suc();
	    iter_suc++) {
	  Edge *edge = *iter_suc;
	  Block *target = edge->target();
	  if(target->id() == id_last) {
	    tmp_v.push_back(b);
	    break;
	  }
	}
        
      }
      return tmp_v;
    }
    
    unsigned long compute_total_iteration(Block *b) {
      // if the loop header exist return the nb of iteration
      Block *bh = Loop::of(b)->header();
      if(bh->isBasic()) {
	return TOTAL_ITERATION(bh->toBasic()->first());
      }

      // if the CFG is the main function
      if(b->cfg()->callers().count() == 0) {
	return 1;
      }

      // if the CFG is a function then we sum the nb of iteration from all of synth block that call it
      unsigned int total = 0;
      for(int i = 0; i < b->cfg()->callers().count(); i++) {
        total += compute_total_iteration(b->cfg()->callers()[i]);
      }
      return total;
    }

	  
    void explo_by_block(Block *b) {
      std::cout << "-------------------" << std::endl;
      std::cout << "Block: " << b->id() << " ";
      if(b->isBasic()) {
	std::cout << "ExecTimeBasic "<< ipet::TIME(b->toBasic()) << std::endl;
	//	if(Loop::of(b)->header() != NULL) {
	  
	std::cout << "total iteration: " << compute_total_iteration(b) << std::endl;
	  
	//	}
	// time_bblock(b->toBasic());
	compute_trivial_lblocks(b->toBasic()); 
	AllocArray<LBlock *> *lblock_array = BB_LBLOCKS(b->toBasic());

	if (lblock_array != NULL)
	  {

	    for (int i = 0; i < lblock_array->count(); i++)
	      {

		LBlock *lb = (*lblock_array)[i];
		//	time_lblock(lb);
		category_t status = cache::CATEGORY(lb);
		const hard::Cache *cache = lb->lblockset()->cache();
		otawa::Address lb_id = cache->round(lb->address());
		//std::cout << ipet::TIME(lb->instruction()) << "--" << cache->line(lb_id) << ":" <<lb_id.page() << ":" << lb_id.offset() << " " ;
		switch(status) {
		case FIRST_HIT:
		  std::cout << "FIRST_HIT" << std::endl;
		  break;
		case FIRST_MISS:
		  std::cout << "FIRST_MISS" << std::endl;
		  break;
		case ALWAYS_HIT:
		  std::cout << "ALWAYS_HIT" << std::endl;
		  break;
		case ALWAYS_MISS:
		  std::cout << "ALWAYS_MISS" << std::endl;
		  break;
		  
		default:
		  std::cout << "NOT_CLASSIFIED" << std::endl;
		  break;
		}
	      }
	  }
      } else {
	if(b->isSynth()) {
	  std::cout << "Synth " << std::endl;
	}
	else {
	  std::cout << std::endl;
	}
      }
      
      for(Block::EdgeIter iter_suc(b->outs());
	  iter_suc();
	  iter_suc++) {
					
	Edge *edge = *iter_suc;
	Block *target = edge->target();
	elm::Pair<long int, otawa::ilp::Var*> p = etime::HTS_CONFIG(edge);
	std::cout << "HTS " << p.fst  << std::endl;
		std::cout << "LTS B" << b->id() <<  "-B" << target->id() << " : " << etime::LTS_TIME(edge) << std::endl;
	std::cout << "B" << b->id() <<  "-B" << target->id() << " : " << ipet::TIME(edge) << std::endl;
	std::cout << "DELTA B" << b->id() <<  "-B" << target->id() << " : " << ipet::TIME_DELTA(edge) << std::endl;
      }
      
      std::cout << std::endl;
      for(Block::EdgeIter iter_pred(b->ins());
      	  iter_pred();
      	  iter_pred++) {
					
      	Edge *edge = *iter_pred;
      	Block *source = edge->source();
	
	elm::Pair<long int, otawa::ilp::Var*> p =etime:: HTS_CONFIG(edge);
	std::cout << "HTS " << p.fst  << std::endl;
	std::cout << "LTS B" << source->id() <<  "-B" << b->id() << " : " << etime::LTS_TIME(edge) << std::endl;
	std::cout << "B" << source->id() <<  "-B" << b->id() << " : " << ipet::TIME(edge) << std::endl;
	std::cout << "DELTA B" << source->id() <<  "-B" << b->id() << " : " << ipet::TIME_DELTA(edge) << std::endl;
      }
      // std::cout << std::endl;
      std::cout << "-------------------" << std::endl;
    }
		
    void explo_cfg(CFG *cfg) {
      std::cout << "#############################################" << std::endl;
      for(CFG::BlockIter bi = cfg->blocks(); bi(); bi++) {
	Block *tmp = *bi;
	explo_by_block(tmp);
      }
      std::cout << "#############################################" << std::endl;
    }
		
    void ExploProcessor::processWorkSpace(WorkSpace *ws) {
      const CFGCollection *coll = INVOLVED_CFGS(ws);
			
      for(CFGCollection::Iter cfg_iter(*coll); cfg_iter(); cfg_iter++) {
	CFG* tmp = *cfg_iter;
	explo_cfg(tmp);
	std::cout << std::endl;
      }
    }



    // definition propriete 
    /*
      Identifier<DAGHNode*> DAG_HNODE("otawa::explo::DAG_HNODE");
    */

  }
}
